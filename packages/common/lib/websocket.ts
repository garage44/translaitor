import EventEmitter from 'eventemitter3'

export const constructMessage = (url, data) => {
    return {data, url}
}

export function parseMessage(message) {
    return JSON.parse(message)
}

export const router = new EventEmitter()
