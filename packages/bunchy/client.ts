import {parseMessage, router} from '@garage44/common/lib/websocket'

function updateStylesheet(filename: string, publicPath: string) {
    const linkElements = document.querySelectorAll(`link[rel=stylesheet]`)
    linkElements.forEach(link => {
        if (link.href.includes(filename)) {
            const newLink = document.createElement('link')
            newLink.rel = 'stylesheet'
            newLink.href = `/${publicPath}/${filename}?${new Date().getTime()}`

            newLink.onload = () => {
                link.remove()
            }

            link.parentNode?.insertBefore(newLink, link.nextSibling)
        }
    })
}

export class BunchyClient {
    constructor() {
        this.socket = new WebSocket(`ws://${window.location.hostname}:3030/bunchy`)
        this.socket.addEventListener('open', function() {
            // eslint-disable-next-line no-console
            console.log('[bunchy] client connected')
        })

        this.socket.addEventListener('message', function(event) {
            const message = parseMessage(event.data)
            router.emit(message.url, message.data)
        })

        this.socket.addEventListener('close', function() {
            // eslint-disable-next-line no-console
            console.log('[bunchy] connection closed')
        })

        this.socket.addEventListener('error', function(event) {
            // eslint-disable-next-line no-console
            console.error(`[bunchy] socket error: ${event}`)
        })
    }
}

router.on('/tasks/code_backend', () => {
    window.location.reload()
})

router.on('/tasks/code_frontend', () => {
    window.location.reload()
})

router.on('/tasks/html', () => {
    window.location.reload()
})

router.on('/tasks/styles/app', ({filename, publicPath}) => {
    updateStylesheet(filename, publicPath)
})

router.on('/tasks/styles/components', ({filename, publicPath}) => {
    updateStylesheet(filename, publicPath)
})
