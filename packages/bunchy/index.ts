import {Scss, generateRandomId, showConfig} from './utils'
import {URL, fileURLToPath} from 'node:url'
import {WebSocketServer} from 'ws'
import {constructMessage} from '@garage44/common/lib/websocket.ts'
import path from 'path'
import {tasks} from './tasks.ts'

const currentDir = fileURLToPath(new URL('.', import.meta.url))
export const connections = new Set<WebSocket>()

interface Settings {
    buildId: string
    dir: {
        assets: string
        bunchy: string
        common: string
        components: string
        public: string
        scss: string
        src: string
        workspace: string
    }
    reload_ignore: string[]
}

export const settings = {} as Settings
export const tooling = {} as {scss: unknown}

async function applySettings(config) {

    Object.assign(settings, {
        buildId: generateRandomId(),
        dir: {
            assets: path.resolve(path.join(config.workspace, 'src', 'assets')),
            bunchy: currentDir,
            common: config.common,
            components: path.resolve(path.join(config.workspace, 'src', 'components')),
            public: path.resolve(path.join(config.workspace, `public`)),
            scss: path.resolve(path.join(config.workspace, 'src', 'scss')),
            src: path.resolve(path.join(config.workspace, 'src')),
            workspace: config.workspace,
        },
        minify: config.minify,
        reload_ignore: config.reload_ignore,
        sourceMap: config.sourceMap,
        version: config.version,
    })
    tooling.scss = Scss(settings)

    showConfig(settings)
}

export async function bunchyService(server, config) {
    applySettings(config)

    // Create WebSocket server attached to HTTP server
    const wss = new WebSocketServer({
        path: '/bunchy',
        server,
    })

    wss.on('connection', (ws) => {
        connections.add(ws)

        ws.on('message', (message) => {
            ws.send(message)
        })

        ws.on('close', () => {
            connections.delete(ws)
        })
    })

    await tasks.dev.start({minify: false, sourceMap: true})
    return server
}

export function bunchyArgs(yargs, config) {
    applySettings(config)

    yargs.option('minify', {
        default: false,
        description: '[Bunchy] Minify output',
        type: 'boolean',
    }).option('sourcemap', {
        default: true,
        description: '[Bunchy] Include source mapping',
        type: 'boolean',
    }).option('builddir', {
        default: '',
        describe: '[Bunchy] Directory to build to',
        type: 'string',
    }).command('build', '[Bunchy] build application', async(yargs) => {
        applySettings({...config, minify: yargs.argv.minify, sourceMap: yargs.argv.sourcemap})
        tasks.build.start({minify: true, sourceMap: true})
    }).command('code_backend', '[Bunchy] bundle backend javascript', (yargs) => {
        applySettings({...config, minify: yargs.argv.minify, sourceMap: yargs.argv.sourcemap})
        tasks.code_backend.start({minify: true, sourceMap: true})
    }).command('code_frontend', '[Bunchy] bundle frontend javascript', (yargs) => {
        applySettings({...config, minify: yargs.argv.minify, sourceMap: yargs.argv.sourcemap})
        tasks.code_frontend.start({minify: true, sourceMap: true})
    }).command('html', '[Bunchy] build html file', (yargs) => {
        applySettings({...config, minify: yargs.argv.minify, sourceMap: yargs.argv.sourcemap})
        tasks.html.start({minify: true, sourceMap: true})
    }).command('styles', '[Bunchy] bundle styles', (yargs) => {
        applySettings({...config, minify: yargs.argv.minify, sourceMap: yargs.argv.sourcemap})
        tasks.styles.start({minify: true, sourceMap: true})
    })

    return yargs
}

export const broadcast = (url: string, data: unknown) => {
    const message = constructMessage(url, data)
    for (const ws of connections) {
        if (ws.readyState=== WebSocket.OPEN) {
            ws.send(JSON.stringify(message))
        }
    }
}
