import {WebSocket, WebSocketServer} from 'ws'
import {Server} from 'http'
import {constructMessage} from '@garage44/common/lib/websocket.ts'

export const connections = new Set()

export const initWebSocketServer = (app) => {
    const server = new Server(app)
    const wss = new WebSocketServer({path: '/ws', server})

    wss.on('connection', (ws) => {
        connections.add(ws)

        ws.on('message', (message) => {
            ws.send(message)
        })

        ws.on('close', () => {
            connections.delete(ws)
        })
    })

    return server
}

export const broadcast = (url: string, data: unknown) => {
    const message = constructMessage(url, data)
    for (const ws of connections) {
        if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(message))
        }
    }
}
