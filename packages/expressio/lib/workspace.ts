import {I18n, WorkspaceConfig} from '../src/types.ts'
import {
    copyObject,
    keyMod,
    keyPath,
    mergeDeep,
    sortNestedObjectKeys,
} from '@garage44/common/lib/utils.ts'
import {WorkspaceDescription} from '../src/types.ts'
import {broadcast} from '../lib/websocket.ts'
import chokidar from 'chokidar'
import fs from 'fs-extra'
import {glob} from 'glob'
import {lintWorkspace} from './lint.ts'
import {logger} from '../service.ts'
import path from 'node:path'
export class Workspace {

    config:WorkspaceConfig = {
        languages: {
            source: 'eng-gbr', // default source language for new workspaces
            target: [],
        },
        source_file: '',
        sync: {
            dir: '${workspaceFolder}/src/**/*.{ts,tsx}',
            enabled: false,
        },
        workspace_id: '',
    }

    i18n:I18n = {}

    private watcher: chokidar.FSWatcher

    async init(description: WorkspaceDescription, isService = true) {
        this.config.source_file = description.source_file
        const configExists = await fs.pathExists(this.config.source_file)

        if (!configExists) {
            // The workspace_id is provided when creating a new workspace
            this.config.workspace_id = description.workspace_id

            logger.info(`[workspace-${this.config.workspace_id}] no workspace config found yet; creating ${this.config.source_file}...`)
            await fs.writeFile(this.config.source_file, JSON.stringify(sortNestedObjectKeys({
                config: this.config,
                i18n: {},
            }), null, 4), 'utf8')
        }

        const workspaceData = await this.load()
        this.config.workspace_id = workspaceData.config.workspace_id

        mergeDeep(this.config, workspaceData.config)
        this.i18n = workspaceData.i18n

        if (isService && this.config.sync.enabled) {
            await this.watch()
        }
    }

    async load() {
        const loadedSettings = JSON.parse((await fs.readFile(this.config.source_file, 'utf8')))
        const i18n = loadedSettings.i18n

        // Augment with state keys
        keyMod(i18n, (_srcRef, _id, refPath) => {
            const key = refPath[refPath.length - 1]
            const sourceRef = keyPath(i18n, refPath)

            if (typeof sourceRef === 'object') {
                // The _id field is a copy of the key, used to buffer a key rename.
                sourceRef._id = key ? key : 'root'
                sourceRef._collapsed = key ? true : false
            }
        })

        return loadedSettings
    }

    async save() {
        const i18n = copyObject(this.i18n)
        const config = copyObject(this.config)
        delete config.source_file

        keyMod(i18n, (_srcRef, _id, refPath) => {
            const sourceRef = keyPath(i18n, refPath)
            if (typeof sourceRef === 'object') {
                // Soft translation tags shall not pass
                if ('source' in sourceRef && '_soft' in sourceRef) {
                    const parentRef = keyPath(i18n, refPath.slice(0, -1))
                    delete parentRef[sourceRef._id]
                }

                // Strip all temporary state keys, before saving.
                delete sourceRef._id
                delete sourceRef._collapsed
                delete sourceRef._redundant
            }
        })

        await fs.writeFile(this.config.source_file, JSON.stringify(sortNestedObjectKeys({
            config: this.config,
            i18n,
        }), null, 4), 'utf8')
    }

    async unwatch() {
        logger.info(`[workspace-${this.config.workspace_id}] unwatch workspace`)
        await this.watcher.close()
    }

    async watch() {
        const scan_target = this.config.sync.dir.replace('${workspaceFolder}', path.dirname(this.config.source_file))
        logger.info(`[workspace-${this.config.workspace_id}] watch ${scan_target}`)
        const files = await glob(scan_target)
        this.watcher = chokidar.watch(files)
        this.watcher.on('ready', () => {
            const watched = this.watcher.getWatched()
            const fileCount = Object.values(watched)
                .reduce((sum, files) => sum + files.length, 0)
            logger.info(`[workspace-${this.config.workspace_id}] watching ${fileCount} files`)
        })
        this.watcher.on('change', async() => {
            const lintResult = await lintWorkspace(this, 'sync')
            if (lintResult) {
                broadcast('/i18n/sync', lintResult)
            }
        })

        const lintResult = await lintWorkspace(this, 'sync')
        if (lintResult) {
            broadcast('/i18n/sync', lintResult)
        }
    }
}
