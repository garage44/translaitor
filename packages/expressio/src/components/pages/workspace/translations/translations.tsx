import {pathCreate, pathDelete, pathUpdate} from '@garage44/common/lib/paths.ts'
import {$s} from '@/app'

import type {EnolaTag} from '@garage44/enola/types.ts'
import {TranslationGroup} from '@/components/elements'
import {router} from '@garage44/common/lib/websocket'

// Define interface for the tag object
interface TagData {
    path: string[];
    value: EnolaTag;
}

router.on('/i18n/sync', ({create_tags, delete_tags, modify_tags}) => {
    for (const tag of create_tags) {
        const {path, value}: TagData = tag
        console.log('PATH CREATE', value.target)
        pathCreate($s.workspace.i18n, path, value, $s.workspace.config.languages.target, value.target)
    }
    for (const tag of delete_tags) {
        const {path}: {path: string[]} = tag
        pathDelete($s.workspace.i18n, path)
    }

    for (const tag of modify_tags) {
        const {path, value}: TagData = tag
        pathUpdate($s.workspace.i18n, path, value)
    }
})

export function WorkspaceTranslations() {
    // Component is not rendered while the workspace is not set
    if (!$s.workspace) return null

    return (
        <div class="c-translations view">
            <TranslationGroup
                group={$s.workspace.i18n}
                path={[]}
            />
        </div>
    )
}
