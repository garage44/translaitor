import {FieldText, Icon} from '@garage44/common/components'
import {pathMove, pathRef} from '@garage44/common/lib/paths'
import {$s} from '@/app'
import {$t} from '@garage44/common/app'
import {TranslationResult} from '@/components/elements'
import {api} from '@garage44/common/app'
import {classes} from '@garage44/common/lib/utils'
import {mergeDeep} from '@garage44/common/lib/utils'
import {tag_updated} from '@/lib/ui'

export function Translation({group, path}) {

    const path_update = path.join('.')

    async function translate() {
        const result = await api.post(`/api/workspaces/${$s.workspace.config.workspace_id}/translate`, {
            path,
            value: group,
        })

        const {id, ref} = pathRef($s.workspace.i18n, path)
        // Translating a soft tag will make it a permanent tag.
        if ('_soft' in ref[id]) {
            delete ref[id]._soft
        }

        $s.usage.loading = true
        setTimeout(async() => {
            // There has to be some time between translating and
            // requesting usage from Deepl, or the usage is not updated.
            const usage = await api.get('/api/usage')
            mergeDeep($s.usage, usage, {loading: false})
        }, 2000)

        Object.assign(group, result)
        group._collapsed = false
        tag_updated(path_update)
    }

    return <div class={classes('c-translation', {
        redundant: group._redundant,
        soft: group._soft,
        'tag-updated': $s.tags.updated === path_update})}>
        <div class="source">

            <div class="tag-actions">
                <Icon
                    name={group._collapsed ? 'eye_off' : 'eye'}
                    onClick={() => {
                        group._collapsed = !group._collapsed
                    }}
                    size="s"
                    tip={$t('translation.tip.translation_view')}
                    type={Object.keys(group.target).length === $s.workspace.config.languages.target.length ? "success" : 'warning'}
                />
                <Icon
                    name="translate"
                    onClick={translate}
                    size="s"
                    tip={$t('translation.tip.translate')}
                    type="info"
                />
                <Icon
                    name="trash"
                    onClick={async() => {
                        const {id, ref} = pathRef($s.workspace.i18n, path)
                        delete ref[id]

                        await api.delete(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {
                            path,
                        })
                    }}
                    size="s"
                    tip={$t('translation.tip.remove')}
                    type="info"
                />
            </div>

            <FieldText
                className="id-field"
                model={group.$_id}
                onBlur={async() => {
                    const oldPath = [...path]
                    const newPath = [...oldPath.slice(0, -1), group._id]
                    if (oldPath.join('.') !== newPath.join('.')) {
                        pathMove($s.workspace.i18n, oldPath, newPath)

                        await api.post(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {
                            new_path: newPath,
                            old_path: oldPath,
                        })
                        tag_updated(newPath.join('.'))
                    }
                }}
                transform={(modelValue) => {
                    modelValue = modelValue.toLocaleLowerCase().replaceAll(' ', '_')
                    return modelValue
                }}
            />
            <FieldText
                className="src-field"
                model={group.$source}
                onBlur={async() => {
                    await api.put(`/api/workspaces/${$s.workspace.config.workspace_id}/tags`, {
                        path,
                        source: group.source,
                    })
                    // const oldPath = path
                    // const newPath = [...path.slice(0, -1), group._id]
                    // if (oldPath.join('.') !== newPath.join('.')) {
                    //     pathMove($s.workspace.i18n, oldPath, newPath)
                    // tag_updated(newPath.join('.'))
                }}
                onKeyDown={(e) => {
                    if (e.key === 'Enter' && e.ctrlKey) {
                        translate()
                    } else if (e.key === ' ' && e.ctrlKey) {
                        group._collapsed = !group._collapsed
                    }
                }}
                placeholder={$t('translation.placeholder.translate')}
            />
        </div>
        <TranslationResult group={group} />
    </div>
}
