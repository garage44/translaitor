import {$t, api, notify} from '@garage44/common/app'
import {FieldText, Icon} from '@garage44/common/components'
import {classes, keyPath, randomId} from '@garage44/common/lib/utils'
import {pathCreate, pathHas, pathMove, pathRef, pathToggle} from '@garage44/common/lib/paths'
import {$s} from '@/app'
import {Translation} from '@/components/elements'
import {tag_updated} from '@/lib/ui'

interface TranslationGroupType {
    _id: string
    source?: string
    [key: string]: unknown
}

export function TranslationGroup({group, level = 0, path}: {
    group: TranslationGroupType
    level?: number
    path: string[]
}) {

    return <div class={classes('c-translation-group', `level-${level}`, {
        collapsed: group._collapsed,
        'has-redundant': pathHas($s.workspace.i18n, path, '_redundant'),
        'has-soft': pathHas($s.workspace.i18n, path, '_soft'),
        'tag-updated': $s.tags.updated === path.join('.')},
    )}>
        <div class="group-id">
            <div class="actions">

                <div class="group-actions">
                    {level > 0 && <Icon
                        name={(() => {
                            if ($s.env.ctrlKey) {
                                return 'plus_collaped'
                            } else if ($s.env.shiftKey) {
                                return 'plus_extra'
                            } else if (group._collapsed) {
                                return 'plus'
                            } else {
                                return 'minus'
                            }
                        })()}
                        onClick={(e) => {
                            // Force uncollapse on Ctrl or Alt click. Ctrl-click will also
                            // make the source tags uncollapse.
                            const forceUncollapse = e.ctrlKey || e.shiftKey
                            const newCollapsedState = forceUncollapse ? false : !group._collapsed
                            // Ctrl-click will uncollapse everything, Alt-click or collapsed state will collapse source tags
                            const tagModifier = e.ctrlKey ? {_collapsed: false} : e.shiftKey || newCollapsedState? {_collapsed: true} : null
                            pathToggle($s.workspace.i18n, path, {_collapsed: newCollapsedState}, tagModifier)
                        }}
                        size="s"
                        tip={group._collapsed ? $t('translation.tip.translation_view') : $t('translation.tip.translation_hide')}
                        type="info"
                    />}
                    <Icon
                        name="group_add"
                        onClick={async() => {
                            const id = `group_${randomId()}`
                            const targetPath = [...path, id]
                            const refPath = keyPath($s.workspace.i18n, path)

                            // Add a new group
                            refPath[id] = {_id: id}
                            await api.put(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {
                                path: targetPath,
                                value: {},
                            })
                            tag_updated(targetPath.join('.'))
                        }}
                        size="s"
                        tip={$t('translation_group.tip.add_group')}
                        type="info"
                    />
                    {path.length > 0 && <Icon
                        name="trash"
                        onClick={async() => {
                            // Remove the group
                            const {id, ref} = pathRef($s.workspace.i18n, path)
                            delete ref[id]
                            await api.delete(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {path})
                        }}
                        size="s"
                        tip={$t('translation_group.tip.remove_group')}
                        type="info"
                    />}
                </div>

                <div class="group-tag-actions">
                    <Icon
                        name="plus"
                        onClick={async() => {
                            const id = `tag_${randomId()}`
                            const targetPath = [...path, id]

                            // Create a new tag
                            const {ref} = pathCreate($s.workspace.i18n, targetPath, {
                                _id: id,
                                source: id,
                            }, $s.workspace.config.languages.target)

                            tag_updated(targetPath.join('.'))

                            await api.put(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {
                                path: targetPath,
                                value: ref[id],
                            })
                        }}
                        size="s"
                        tip={$t('translation_group.tip.add_tag')}
                        type="info"
                    />
                    <Icon
                        name="translate"
                        onClick={async() => {
                            // Translate the group
                            const result = await api.post(`/api/workspaces/${$s.workspace.config.workspace_id}/translate`, {
                                ignore_cache: $s.env.ctrlKey,
                                path,
                            })

                            if (result.cached.length) {
                                notify({message: $t('notifications.translate_group_cached', {count: result.cached.length}), type: 'info'})
                            }
                            if (result.translations.length) {
                                notify({message: $t('notifications.translate_group_translated', {count: result.translations.length}), type: 'success'})
                            }

                            if (!result.cached && !result.translations.length) {
                                notify({message: $t('notifications.translate_group_failed', {count: result.failed.length}), type: 'error'})
                            }

                            Object.assign(group, result.ref)
                            pathToggle($s.workspace.i18n, path, {_collapsed: false}, true)
                            tag_updated(path.join('.'))
                        }}
                        size="s"
                        tip={$s.env.ctrlKey ? $t('translation.tip.translate_group_force') : $t('translation.tip.translate_group')}
                        type={$s.env.ctrlKey ? "danger" : "info"}
                    />
                </div>
            </div>

            {path.length > 0 && <FieldText
                className="group-field"
                model={group.$_id}
                onBlur={async() => {
                    const oldPath = path
                    const newPath = [...path.slice(0, -1), group._id]
                    if (oldPath.join('.') !== newPath.join('.')) {
                        pathMove($s.workspace.i18n, oldPath, newPath)
                        await api.post(`/api/workspaces/${$s.workspace.config.workspace_id}/paths`, {
                            new_path: newPath,
                            old_path: oldPath,
                        })
                    }
                    tag_updated(newPath.join('.'))
                }}
                transform={(value) => value.toLocaleLowerCase().replaceAll(' ', '_')}
            />}

        </div>
        <div class="group-value">
            {group && Object.entries(group)
                .filter(([id]) => !id.startsWith('_'))
                .sort(([idA, groupA], [idB, groupB]) => {
                    const a = groupA as TranslationGroupType
                    const b = groupB as TranslationGroupType
                    if (!('source' in a)) {
                        return 1
                    } else if (!('source' in b)) {
                        return -1
                    } else {
                        return idA.localeCompare(idB)
                    }
                })
                .map(([id, subGroup]) => {
                    const typedSubGroup = subGroup as TranslationGroupType
                    if ('source' in typedSubGroup) {
                        return <Translation
                            group={typedSubGroup}
                            path={[...path, id]}
                        />
                    }

                    return <TranslationGroup
                        level={level + 1}
                        group={typedSubGroup}
                        path={[...path, id]}
                    />
                })
            }
        </div>
    </div>
}
