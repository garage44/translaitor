import {
    App,
    $s as _$s,
    store,
} from '@garage44/common/app'
import {
    ExpressioState,
    persistantState,
    volatileState,
} from './lib/state'
import {h, render} from 'preact'
import {parseMessage, router} from '@garage44/common/lib/websocket'

import {BunchyClient} from '@garage44/bunchy/client'
import {Main} from '@/components/main/main'
import {i18nFormat} from '@garage44/common/lib/i18n.ts'
import workspace from '@/.expressio.json'

// Development client is injected here
if (process.env.NODE_ENV === 'development') {
    new BunchyClient()
}

export const $s = _$s as ExpressioState
store.load(persistantState, volatileState)

export const app = new App()

app.init(Main, render, h, i18nFormat(workspace.i18n, workspace.config.languages.target))
const socket = new WebSocket(`ws://${window.location.hostname}:3030/ws`)

socket.addEventListener('open', function() {
    // eslint-disable-next-line no-console
    console.log('[expressio] websocket connected')
})

socket.addEventListener('message', function(event) {
    const message = parseMessage(event.data)
    router.emit(message.url, message.data)
})

socket.addEventListener('close', function() {
    // eslint-disable-next-line no-console
    console.log('[expressio] websocket closed')
})

socket.addEventListener('error', function(event) {
    // eslint-disable-next-line no-console
    console.log(`[expressio] websocket error: ${event}`)
})
