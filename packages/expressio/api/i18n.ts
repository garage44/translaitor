import {pathCreate, pathDelete, pathMove, pathRef} from '@garage44/common/lib/paths.ts'
import {translate_path, translate_tag} from '../lib/translate.ts'
import {i18nFormat} from '@garage44/common/lib/i18n.ts'
import {logger} from '@garage44/common/app'
import {workspaces} from '../service.ts'

export default async function(app) {

    app.delete('/api/workspaces/:workspace_id/paths', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        const {path} = req.body
        pathDelete(workspace.i18n, path)
        workspace.save()
        res.json({deleted: path})
    })

    app.post('/api/workspaces/:workspace_id/paths', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        const {old_path, new_path} = req.body
        pathMove(workspace.i18n, old_path, new_path)
        workspace.save()
        res.json({moved: {new_path, old_path}})
    })

    app.put('/api/workspaces/:workspace_id/paths', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        const {path, value} = req.body
        const result = pathCreate(workspace.i18n, path, value, workspace.config.languages.target)
        workspace.save()
        res.json(result)
    })

    app.put('/api/workspaces/:workspace_id/tags', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        const {path, source} = req.body
        const {id, ref} = pathRef(workspace.i18n, path)
        ref[id].source = source

        workspace.save()
        res.json(ref[id])
    })

    app.get('/api/workspaces/:workspace_id/i18n', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        if (!workspace) {
            return res.status(404).json({error: 'Workspace not found'})
        }
        res.json(workspace.i18n)
    })

    app.get('/api/workspaces/:workspace_id/translations', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)
        res.json(i18nFormat(workspace.i18n, workspace.config.languages.target))
    })

    app.post('/api/workspaces/:workspace_id/translate', async(req, res) => {
        const workspace = workspaces.get(req.params.workspace_id)

        const ignore_cache = req.body.ignore_cache
        const {id, ref} = pathRef(workspace.i18n, req.body.path)

        if (req.body.value) {
            const sourceText = req.body.value.source
            const persist = !!req.body.value._soft

            try {
                await translate_tag(workspace, req.body.path, sourceText, persist)
                res.json(ref[id])
            } catch (error) {
                logger.error('Translation error:', error)
                return res.status(500).json({error: 'Translation failed'})
            }
        } else {
            try {
                const {cached, targets, translations} = await translate_path(workspace, req.body.path, ignore_cache)
                res.json({cached, ref: ref[id], targets, translations})
            } catch (error) {
                logger.error('Translation error:', error)
                return res.status(500).json({error: 'Translation failed'})
            }
        }

        workspace.save()
    })
}
