import {app} from '@/lib/main.js'

export let localStream
export let screenStream

export async function getUserMedia(presence) {
    app.$s.mediaReady = false
    // Cleanup the old networked stream first:
    if (localStream) {
        if (app.$s.group.connected) {
            app.$m.sfu.delUpMediaKind('camera')
        } else {
            app.$m.sfu.delLocalMedia()
        }
    }

    let selectedAudioDevice = false
    let selectedVideoDevice = false

    if (app.$s.devices.mic.selected.id !== null) {
        selectedAudioDevice = {deviceId: app.$s.devices.mic.selected.id}
    }

    if (app.$s.devices.cam.selected.id !== null) {
        selectedVideoDevice = {deviceId: app.$s.devices.cam.selected.id}
    }

    if (presence) {
        if (!presence.cam.enabled) selectedVideoDevice = false
        if (!presence.mic.enabled) selectedAudioDevice = false
        // A local stream cannot be initialized with neither audio and video; return early.
        if (!presence.cam.enabled && !presence.mic.enabled) {
            app.$s.mediaReady = true
            return
        }
    }

    // Verify whether the local mediastream is using the proper device setup.
    app.logger.debug(`using cam ${app.$s.devices.cam.selected.name}`)
    app.logger.debug(`using mic ${app.$s.devices.mic.selected.name}`)

    if (selectedVideoDevice) {
        if (app.$s.devices.cam.resolution.id === '720p') {
            app.logger.debug(`using 720p resolution`)
            selectedVideoDevice.width = {ideal: 1280, min: 640}
            selectedVideoDevice.height = {ideal: 720, min: 400}
        } else if (app.$s.devices.cam.resolution.id === '1080p') {
            app.logger.debug(`using 1080p resolution`)
            selectedVideoDevice.width = {ideal: 1920, min: 640}
            selectedVideoDevice.height = {ideal: 1080, min: 400}
        }
    }

    const constraints = {
        audio: selectedAudioDevice,
        video: selectedVideoDevice,
    }

    try {
        localStream = await navigator.mediaDevices.getUserMedia(constraints)
    } catch (message) {
        app.notifier.notify({level: 'error', message})
        app.$s.mediaReady = true
        return
    }

    // Add local stream to Galène; handle peer connection logic.
    if (app.$s.group.connected) {
        await app.$m.sfu.addUserMedia()
    }

    app.$s.mediaReady = true
    return localStream
}

export async function queryDevices() {
    app.logger.info('querying for devices')
    // The device labels stay empty when there is no media permission.
    let devices
    if (app.$s.env.isFirefox) {
        // The device labels are only available in Firefox while a stream is active.
        const stream = await navigator.mediaDevices.getUserMedia({audio: true, video: true})
        devices = await navigator.mediaDevices.enumerateDevices()
        for (const track of stream.getTracks()) {
            track.stop()
        }
    } else {
        devices = await navigator.mediaDevices.enumerateDevices()
    }

    const labelnr = {audio: 1, cam: 1, mic: 1}
    const added = []

    app.$s.devices.mic.options = []
    app.$s.devices.cam.options = []
    app.$s.devices.audio.options = []

    for (const device of devices) {
        // The same device may end up in the queryList multiple times;
        // Don't add it twice to the options list.
        if (added.includes(device.deviceId)) {
            continue
        }
        let name = device.label

        if (device.kind === 'videoinput') {
            if (!name) name = `Camera ${labelnr.cam}`
            app.$s.devices.cam.options.push({id: device.deviceId ? device.deviceId : name, name})
            labelnr.cam++
        } else if (device.kind === 'audioinput') {
            if (!name) name = `Microphone ${labelnr.mic}`
            app.$s.devices.mic.options.push({id: device.deviceId ? device.deviceId : name, name})
            labelnr.mic++
        } else if (device.kind === 'audiooutput') {
            // Firefox doesn't support audio output enumeration and setSinkid
            if (!name) name = `Output ${labelnr.audio}`
            app.$s.devices.audio.options.push({id: device.deviceId ? device.deviceId : name, name})
            labelnr.audio++
        }

        added.push(device.deviceId)
    }

    app.logger.debug(`device list updated`)
}

export function removeLocalStream() {
    localStream.getTracks().forEach(track => {
        app.logger.debug(`stopping track ${track.id}`)
        track.stop()
    })

    localStream = null
}

export function setDefaultDevice(useFirstAvailable = true) {
    const invalidDevices = validateDevices()
    const emptyOption = {id: null, name: ''}
    for (const key of Object.keys(app.$s.devices)) {
        if (key === 'audio' && app.$s.env.isFirefox) continue

        if (invalidDevices[key] || app.$s.devices[key].selected.id === null) {
            if (useFirstAvailable && app.$s.devices[key].options.length) {
                app.$s.devices[key].selected = app.$s.devices[key].options[0]
            } else {
                app.$s.devices[key].selected = emptyOption
            }
        }
    }
}

export function setScreenStream(stream) {
    screenStream = stream
}

export function validateDevices() {
    const devices = app.$s.devices
    return {
        audio: !app.$s.env.isFirefox && (!devices.audio.options.length || !devices.audio.options.find((i) => i.id === devices.audio.selected.id)),
        cam: !devices.cam.options.length || !devices.cam.options.find((i) => i.id === devices.cam.selected.id),
        mic: !devices.mic.options.length || !devices.mic.options.find((i) => i.id === devices.mic.selected.id),
    }
}

navigator.mediaDevices.ondevicechange = async() => {
    const oldDevices = JSON.parse(JSON.stringify(app.$s.devices))
    await queryDevices()
    let added = [], removed = []
    for (const deviceType of Object.keys(app.$s.devices)) {
        const _added = app.$s.devices[deviceType].options.filter((i) => !oldDevices[deviceType].options.find((j) => i.id === j.id))
        const _removed = oldDevices[deviceType].options.filter((i) => !app.$s.devices[deviceType].options.find((j) => i.id === j.id))
        if (_added.length) added = added.concat(_added)
        if (_removed.length) removed = removed.concat(_removed)
    }

    if (added.length) {
        app.notifier.notify({
            icon: 'Headset',
            level: 'info',
            list: added.map((i) => i.name),
            message: app.$tc('device.added', added.length),
        })
    }
    if (removed.length) {
        app.notifier.notify({
            icon: 'Headset',
            level: 'warning',
            list: removed.map((i) => i.name),
            message: app.$tc('device.removed', removed.length),
        })
    }
    const invalidDevices = validateDevices()

    if (app.$s.group.connected && Object.values(invalidDevices).some((i) => i)) {
        app.router.push({name: 'conference-group-settings', params: {tabId: 'devices'}})
        app.notifier.notify({
            icon: 'Headset',
            level: 'warning',
            list: Object.entries(invalidDevices)
                .filter(([_, value]) => value)
                .map(([key]) => app.$t(`device.select_${key}_label`)),
            message: app.$tc('device.action_required', removed.length),
        })
        // Don't set a default option; it must be clear that an
        // invalid device option is set while being connected.
        setDefaultDevice(false)
    } else {
        setDefaultDevice(true)
    }

}
