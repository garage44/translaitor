import {app} from '@/lib/main.js'

export function currentGroup() {
    const currentGroup = app.$s.groups.find((i) => i.name === app.$s.group.name)
    // Assume hidden group; use selected group fields as placeholders.
    if (!currentGroup) {
        return app.$s.group
    }

    return currentGroup
}

export async function saveGroup(groupId, data) {
    const group = await app.api.post(`/api/groups/${encodeURIComponent(groupId)}`, data)

    if (group._name === group._newName) {
        app.notifier.notify({level: 'info', message: app.$t('group.action.saved', {group: group._name})})
        app.$s.admin.groups[app.$s.admin.groups.findIndex((g) => g._name === group._name)] = group
    } else {
        app.notifier.notify({
            level: 'info',
            message: app.$t('group.action.renamed', {
                newname: group._newName,
                oldname: group._name,
            }),
        })

        const groupIndex = app.$s.admin.groups.findIndex((g) => g._name === group._name)
        group._name = group._newName
        app.$s.admin.groups[groupIndex] = group
    }

    return group
}

