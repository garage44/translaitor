import {defineComponent} from '@/lib/component'
import {Button} from '@/components/elements'
import {h} from 'vue'
import {$s, $t, app} from '@/lib/main'

export default defineComponent({
    methods: {
        groupRoute(name) {
            if ($s.admin.group) {
                return {name, params: {groupId: $s.admin.group._name, tabId: 'misc'}}
            } else if ($s.groups.length) {
                return {name, params: {groupId: $s.groups[0].name, tabId: 'misc'}}
            } else {
                return {name, params: {tabId: 'misc'}}
            }
        },
        async logout() {
            const context = await this.app.api.get('/api/logout')
            Object.assign($s.admin, context)
            this.$router.push({name: 'admin-login'})
        },
        toggleCollapse() {
            $s.panels.context.collapsed = !$s.panels.context.collapsed
            app.store.save()
        },
        userRoute(name) {
            if ($s.admin.user) {
                return {name, params: {tabId: 'misc', userId: $s.admin.user.id}}
            } else {
                return {name: 'admin-users-settings'}
            }
        },
    },
    render() {
        return (
            <div class="c-admin-controls">
                <div class="navigational-controls">
                    <Button
                        active={this.$route.name === 'admin-groups-settings'}
                        icon="Group"
                        route={this.groupRoute('admin-groups-settings')}
                        tip={$t('group.groups')}
                        variant="menu"
                    />
        
                    <Button
                        active={this.$route.name.startsWith('admin-users')}
                        icon="User"
                        route={this.userRoute('admin-users-settings')}
                        tip={$t('user.users')}
                        variant="menu"
                    />

                    {($s.admin.authenticated && $s.admin.permission) && <Button
                        icon="Logout"
                        tip={$t('user.action.logout')}
                        variant="menu"
                        onClick={this.logout}
                    />}
                </div>
                
                {$s.env.layout === 'desktop' && <Button
                    icon="ViewList"
                    tip={$s.panels.context.collapsed ? $t('ui.panel.expand') : $t('ui.panel.collapse')}
                    variant="toggle"
                    onClick={this.toggleCollapse}
                />}

            </div>
        )
    },
})
