import {Chart, Icon} from '@/components/elements'
import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$t} from '@/lib/main'

export default defineComponent({
    components: {Chart},
    data() {
        return {
            statProps: {
                bitrate: false,
                jitter: false,
                loss: false,
                maxBitrate: false,
            },
            stats: {
                clients: {},
            },
        }
    },
    methods: {
        async loadStats() {
            let initClient = false
            const stats = await this.app.api.get(`/api/dashboard/${this.groupId}`)
            if (!stats.clients) {
                this.stats.clients = {}
                return
            }

            const clients = stats.clients.map((i) => i.id)

            if (!this.stats) {
                initClient = true
                this.stats = stats
            }

            const removedClients = Object.keys(this.stats.clients).filter((i) => !clients.includes(i))
            // A client was removed.
            for (const clientId of removedClients) {
                this.app.logger.info(`remove client ${clientId}`)
                delete this.stats.clients[clientId]
            }

            if (!Array.isArray(stats.clients)) {
                return
            }

            for (const client of stats.clients) {
                if (!client.up) continue

                if (!this.stats.clients[client.id]) {
                    this.stats.clients[client.id] = client
                    this.stats.clients[client.id].collapsed = true
                    initClient = true
                }

                if (!this.stats.clients[client.id].up || !this.stats.clients[client.id].up.length === client.up.length) {
                    this.stats.clients[client.id].up = JSON.parse(JSON.stringify(client.up))
                    initClient = true
                }

                for (const [streamIndex, stream] of client.up.entries()) {
                    if (stream.tracks.length !== this.stats.clients[client.id].up[streamIndex].tracks.length) {
                        this.stats.clients[client.id].up[streamIndex].tracks = JSON.parse(JSON.stringify(stream.tracks))
                        initClient = true
                    }

                    for (let [trackIndex, track] of stream.tracks.entries()) {
                        let trackRef = this.stats.clients[client.id].up[streamIndex].tracks
                        if (initClient) {
                            trackRef[trackIndex] = {
                                bitrate: [track.bitrate],
                                jitter: [track.jitter],
                                loss: [track.loss],
                                maxBitrate: [track.maxBitrate],
                            }

                        } else {
                            if (this.statEnabled(trackRef[trackIndex], 'bitrate')) this.statProps.bitrate = true
                            trackRef[trackIndex].bitrate.push(track.bitrate)

                            if (this.statEnabled(trackRef[trackIndex], 'jitter')) this.statProps.jitter = true
                            trackRef[trackIndex].jitter.push(track.jitter)

                            if (this.statEnabled(trackRef[trackIndex], 'loss')) this.statProps.loss = true
                            trackRef[trackIndex].loss.push(track.loss)

                            if (this.statEnabled(trackRef[trackIndex], 'maxBitrate')) this.statProps.maxBitrate = true
                            trackRef[trackIndex].maxBitrate.push(track.maxBitrate)
                        }

                    }
                }
            }
        },
        statEnabled(track, property) {
            // Already enabled; return quick.
            if (this.statProps[property]) return true
            if (track[property].find((i) => i !== track[property][0])) return true
            return false
        },
        statsPoller() {
            this.intervalId = setInterval(this.loadStats, 250)
        },
        toggleCollapse(client) {
            client.collapsed = !client.collapsed
        },
    },
    mounted() {
        this.loadStats(this.groupId)
        this.statsPoller()
    },
    unmounted() {
        clearInterval(this.intervalId)
    },
    props: {
        groupId: {
            default: () => null,
            required: false,
            type: String,
        },
    },
    render() {
        if (this.stats && Object.keys(this.stats.clients).length) {
            return (
                <section class="c-admin-groups-stats tab-content active">
                    {this.stats.clients.map((client) => 
                        <div class="client">
                            <div class={classes('client-header', {collapsed: client.collapsed})} onClick={this.toggleCollapse(client)}>
                                <Icon class="icon icon-d" name="Stats" /> {this.client.id}
                            </div>
                            {!client.collapsed && client.up.map((stream) => 
                                <div class="stream">
                                    {stream.tracks.map((track) => <div class="track">
                                        {track.map((data, name) => 
                                        {this.statProps[name] && <Chart data={data} name={name} />},                                            
                                        )}
                                    </div>)}
                                </div>,
                            )}
                        </div>,
                    )}
                </section>
            )
        }

        return (
            <section class="c-admin-dashboard tab-content no-results">
                <Icon class="icon icon-l" name="Stats" />
                <span>{$t('group.settings.statistic.no_connections')}</span>
            </section>
        )
    },
    watch: {
        groupId(newValue) {
            this.loadStats(newValue)
        },
    },
})
