import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'
import {FieldCheckbox, FieldMultiSelect, FieldNumber, FieldText} from '@/components/elements'

export default defineComponent({
    data() {
        return {
            codecs: [
                {
                    help: $t('group.settings.misc.codec_vp8_help'),
                    id: 'vp8',
                    name: 'VP8',
                },
                {
                    help: $t('group.settings.misc.codec_vp9_help'),
                    id: 'vp9',
                    name: 'VP9',
                },
                {
                    help: $t('group.settings.misc.codec_av1_help'),
                    id: 'av1',
                    name: 'AV1',
                },
                {
                    help: $t('group.settings.misc.codec_h264_help'),
                    id: 'h264',
                    name: 'H264',
                },
                {
                    help: $t('group.settings.misc.codec_opus_help'),
                    id: 'opus',
                    name: 'Opus',
                },
                {
                    help: $t('group.settings.misc.codec_g722_help'),
                    id: 'g722',
                    name: 'G722',
                },
                {
                    help: $t('group.settings.misc.codec_pcmu_help'),
                    id: 'pcmu',
                    name: 'PCMU',
                },
                {
                    help: $t('group.settings.misc.codec_pcma_help'),
                    id: 'pcma',
                    name: 'PCMA',
                },
            ],
        }
    },
    render() {
        return (
            <section class="c-admin-group-tab-misc tab-content active">
                <FieldText
                    v-model={$s.admin.group._newName}
                    help={$t('group.settings.misc.name_help')}
                    label={$t('group.settings.misc.name_label')}
                    placeholder="..."
                />
                <FieldText
                    v-model="$s.admin.group.description"
                    help={$t('group.settings.misc.description_help')}
                    label={$t('group.settings.misc.description_label')}
                    placeholder="..."
                />
                <FieldText
                    v-model="$s.admin.group.contact"
                    help={$t('group.settings.misc.contact_help')}
                    label={$t('group.settings.misc.contact_label')}
                    placeholder="..."
                />
                <FieldText
                    v-model={$s.admin.group.comment}
                    help={$t('group.settings.misc.comment_help')}
                    label={$t('group.settings.misc.comment_label')}
                    placeholder="..."
                />
                <FieldCheckbox
                    v-model={$s.admin.group['allow-recording']}
                    help={$t('group.settings.misc.recording_help')}
                    label={$t('group.settings.misc.recording_label')}
                />
                <FieldMultiSelect
                    v-model={$s.admin.group.codecs}
                    help={$t('group.settings.misc.codec_help')}
                    label={$t('group.settings.misc.codec_label')}
                    options={this.codecs}
                />
                <FieldNumber
                    v-model={$s.admin.group['max-history-age']}
                    help={$t('group.settings.misc.chat_history_help')}
                    label={$t('group.settings.misc.chat_history_label')}
                />
            </section>
        )
    },
})
