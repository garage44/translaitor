import {defineComponent} from '@/lib/component'
import {FieldCheckbox, FieldNumber} from '@/components/elements'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    render() {
        return (
            <section class="c-admin-group-tab-access tab-content active">
                <FieldCheckbox
                    v-model={$s.admin.group.public}
                    help={$t('group.settings.access.public_group_help')}
                    label={$t('group.settings.access.public_group_label')}
                />
                <FieldCheckbox
                    v-model={$s.admin.group['public-access']}
                    help={$t('group.settings.access.guest_login_help')}
                    label={$t('group.settings.access.guest_login_label')}
                />
                {$s.admin.group['public-access'] && <FieldCheckbox
                    v-model={$s.admin.group['allow-anonymous']}
                    help={$t('group.settings.access.anonymous_login_help')}
                    label={$t('group.settings.access.anonymous_login_label')}
                />}

                <FieldCheckbox
                    v-model={$s.admin.group['allow-subgroups']}
                    help={$t('group.settings.access.subgroups_help')}
                    label={$t('group.settings.access.subgroups_label')}
                />
        
                <FieldCheckbox
                    v-model={$s.admin.group.autolock}
                    help={$t('group.settings.access.autolock_help')}
                    label={$t('group.settings.access.autolock_label')}
                />
        
                <FieldCheckbox
                    v-model={$s.admin.group.autokick}
                    help={$t('group.settings.access.autokick_help')}
                    label={$t('group.settings.access.autokick_label')}
                />
        
                <FieldNumber
                    v-model={$s.admin.group['max-clients']}
                    help={$t('group.settings.access.maxclient_help')}
                    label={$t('group.settings.access.maxclient_label')}
                    placeholder="..."
                />
            </section>
        )
    },
})
