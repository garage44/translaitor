import {classes} from '@/lib/utils'
import {h} from 'vue'
import {defineComponent} from '@/lib/component'
import Recordings from '@/components/admin/group/recordings/recordings'
import Stats from '@/components/admin/group/stats/stats'
import TabAccess from './tab-access'
import TabMisc from './tab-misc'
import TabPermissions from './tab-permissions'
import {RouterLink} from 'vue-router'
import {Icon} from '@/components/elements'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    components: {Recordings, Stats, TabAccess, TabMisc, TabPermissions},
    computed: {
        currentGroup() {
            return this.$m.group.currentGroup()
        },
        tabId() {
            return this.$route.query.tab || 'misc'
        },
    },
    methods: {
        routeSettings(tabId) {
            return {
                params: {groupId: this.groupId},
                query: {tab: tabId},
                to: 'admin-groups-settings',
            }
        },
        async saveGroup() {
            const group = await this.$m.group.saveGroup(this.groupId, $s.admin.group)
            this.$router.push({name: 'admin-groups-settings', params: {groupId: group._name}, query: this.$route.query})
        },
    },
    props: {
        groupId: {
            default: () => null,
            required: false,
            type: String,
        },
    },
    render() {
        return (
            <div class="c-admin-group content">
                <header>
                    <div class="notice" />
                    <div class="title">
                        {$s.admin.group && <span>{$s.admin.group._name}</span>}                        
                        <Icon class="icon icon-regular" name="Group" />
                    </div>
                </header>
        
                <ul class="tabs">
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {active: this.tabId === 'misc'})}
                        to={this.routeSettings('misc')}
                    >
                        <Icon v-tip={{content: $t('group.settings.misc.name')}} class="icon-d" name="Pirate" />
                    </RouterLink>
        
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {active: this.tabId === 'access'})}
                        to={this.routeSettings('access')}
                    >
                        <Icon v-tip="{content: $t('group.settings.access.name')}" class="icon-d" name="Access" />
                    </RouterLink>
        
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {active: this.tabId === 'permissions'})}
                        to={this.routeSettings('permissions')}
                    >
                        <Icon v-tip={{content: $t('group.settings.permissions.name')}} class="icon-d" name="Operator" />
                    </RouterLink>
        
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {active: this.tabId === 'stats'})}
                        disabled={!$s.group._unsaved}
                        to={this.routeSettings('stats')}
                    >
                        <Icon v-tip={{content: $t('group.settings.statistic.name')}} class="icon-d" name="Stats" />
                    </RouterLink>
        
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {active: this.tabId === 'recordings'})}
                        to={this.routeSettings('recordings')}
                    >
                        <Icon v-tip={{content: $t('group.recording.name')}} class="icon-d" name="Record" />
                    </RouterLink>
                </ul>
        
                <div class="tabs-content">
                    {(() => {
                        if (this.tabId === 'misc') return <TabMisc />
                        if (this.tabId === 'access') return <TabAccess />
                        if (this.tabId === 'permissions') return <TabPermissions />
                        if (this.tabId === 'stats') return <Stats group-id={this.groupId} />
                        if (this.tabId === 'recordings') return <Recordings group-id={this.groupId} />
                    })()}

                    {this.$route.name === 'admin-groups-settings' && <div class="actions">
                        <button
                            class="btn btn-menu btn-save"
                            onClick={this.saveGroup}
                        >
                            <Icon v-tip={{content: $t('group.action.save')}} class="icon-d" name="Save" />
                        </button>
                    </div>}
                </div>
            </div>
        )
    },
})
