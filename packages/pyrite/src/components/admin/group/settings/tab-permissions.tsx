import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {h} from 'vue'
import {$s} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            // (!) Config directives; not the same as permissions from $s.permissions
            categories: ['op', 'presenter', 'other'],
        }
    },
    methods: {
        async loadGroups() {
            $s.admin.groups = await this.app.api.get('/api/groups')
        },
        toggleCategory(category) {
            const allSelected = !$s.admin.users.some((i) => !$s.admin.group._permissions[category].includes(i.name))
            if (allSelected) {
                $s.admin.group._permissions[category] = []
            } else {
                $s.admin.group._permissions[category] = $s.admin.users.map((i) => i.name)
            }
        },
        toggleUser(username) {
            const allSelected = this.categories.every((c) => $s.admin.group._permissions[c].includes(username))
            if (allSelected) {
                for (const category of this.categories) {
                    const userIndex = $s.admin.group._permissions[category].indexOf(username)
                    $s.admin.group._permissions[category].splice(userIndex, 1)
                }
            } else {
                for (const category of this.categories) {
                    if (!$s.admin.group._permissions[category].includes(username)) {
                        $s.admin.group._permissions[category].push(username)
                    }
                }
            }
            for (const category of this.categories) {
                $s.admin.group._permissions[category].includes(username)
            }
        },
    },
    async mounted() {
        if ($s.admin.authenticated && $s.admin.permission) {
            this.loadGroups()
        }
    },
    render() {
        return (
            <section class="c-admin-group-tab-permissions tab-content permissions active">
                <div class="permission-group">
                    <div class="group-name" />
                    <div class="categories">
                        <div class="category" onClick="toggleCategory('op')">
                            <Icon v-tip="{content: $t('group.settings.permission.operator')}" class="icon-d" name="Operator" />
                        </div>
                        <div class="category" onClick="toggleCategory('presenter')">
                            <Icon v-tip="{content: $t('group.settings.permission.presenter')}" class="icon-d" name="Present" />
                        </div>
                        <div class="category" onClick="toggleCategory('other')">
                            <Icon v-tip="{content: $t('group.settings.permission.misc')}" class="icon-d" name="OtherPermissions" />
                        </div>
                    </div>
                </div>

                {$s.admin.users.map((user) => 
                    <div class="permission-group item">
                        <div class="group-name" onClick={this.toggleUser(user.name)}>
                            {user.name}
                        </div>
        
                        <div class="categories">
                            {this.categories.map((category) => 
                                <div class="category">
                                    <input v-model={$s.admin.group._permissions[category]} type="checkbox" value={user.name}/>
                                </div>,
                            )}
                        </div>
                    </div>,
                )}

            </section>
        )
    },
})

