import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            recordings: [],
        }
    },
    methods: {
        async deleteRecording(rec) {
            this.recordings = await this.app.api.get(`/api/recordings/${this.groupId}/${rec.filename}.${rec.extension}/delete`)
            this.app.notifier.notify({level: 'info', message: $t('group.recording.deleted', {recording: rec.filename})})
        },
        async loadRecordings(groupId) {
            this.recordings = await this.app.api.get(`/api/recordings/${groupId}`)
        },
    },
    mounted() {
        this.loadRecordings(this.groupId)
    },
    props: {
        groupId: {
            default: () => null,
            required: false,
            type: String,
        },
    },
    render() {
        if (!this.recordings.length) return (
            <section class="c-admin-recordings tab-content active no-results">
                <Icon class="icon icon-l" name="Record" />
                <span>{$t('group.recording.no_recordings')}</span>
            </section>
        )

        return (
            <section class="c-admin-recordings tab-content active">
                {this.recordings.map((rec) => 
                    <div class="recording">
                        <div class="actions">
                            <button
                                class="btn btn-menu btn-small"
                                onClick={this.deleteRecording(rec)}
                            >
                                <Icon v-tip={{content: $t('group.recording.delete')}} class="icon-s" name="Trash" />
                            </button>
                            <a
                                class="btn btn-menu btn-small"
                                download={`${rec.filename}.${rec.extension}`}
                                href={`/api/recordings/${$s.admin.group._name}/${rec.filename}.${rec.extension}`}
                            >
                                <Icon v-tip="{content: $t('group.recording.download')}" class="icon-s" name="Download" />
                            </a>
                        </div>
                        <video controls src={`/api/recordings/${$s.admin.group._name}/${rec.filename}.${rec.extension}`} type="video/webm" />
                        <div class="info">
                            <div class="line">
                                <div class="key">
                                    {$t('file.name')}
                                </div>
                                <div class="value">
                                    {rec.filename}
                                </div>
                            </div>
            
                            <div class="line">
                                <div class="key">
                                    {$t('type.type')}
                                </div>
                                <div class="value">
                                    {rec.extension}
                                </div>
                            </div>
            
                            <div class="line">
                                <div class="key">
                                    {$t('file.size')}
                                </div>
                                <div class="value">
                                    {(rec.size / 1024 / 1024).toFixed(2)} MB
                                </div>
                            </div>
            
                            <div class="line">
                                <div class="key">
                                    {$t('file.modified')}
                                </div>
                                <div class="value">
                                    {rec.atime}
                                </div>
                            </div>
                        </div>
                    </div>,
                )}
                
            </section>

        )
    },
    watch: {
        groupId(newValue) {
            this.loadRecordings(newValue)
        },
    },
})
