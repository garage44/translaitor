import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t, $tc} from '@/lib/main'

export default defineComponent({
    computed: {
        deletionGroups() {
            return $s.admin.groups.filter((i) => i._delete)
        },
        /**
         * List the non-public groups at the bottom, so the group list
         * at the upper side is the same as the public group list
         * for users.
         */
        orderedGroups() {
            const groups = $s.admin.groups
                .filter((g) => g.public).concat($s.admin.groups.filter((g) => !g.public))
            return groups.sort((a, b) => {
                if ( a._name < b._name) return -1
                if ( a._name > b._name) return 1
                return 0
            })
        },
    },
    data() {
        return {
            selected: null,
        }
    },
    methods: {
        async addGroup() {
            const group = await this.app.api.get('/api/groups/template')
            $s.admin.groups.push(group)
            this.toggleSelection(group._name)
        },
        async deleteGroups() {
            this.app.notifier.notify({level: 'info', message: $tc('deleting one group | deleting {count} groups', this.deletionGroups.length)})
            const deleteRequests = []
            for (const group of this.deletionGroups) {
                $s.admin.groups.splice($s.admin.groups.findIndex((i) => i._name === group._name), 1)
                if (!group._unsaved) {
                    deleteRequests.push(fetch(`/api/groups/${group._name}/delete`))
                }
            }

            await Promise.all(deleteRequests)

            if (this.orderedGroups.length) {
                const groupId = this.orderedGroups[0]._name
                this.$router.push({name: 'admin-groups-settings', params: {groupId, tabId: 'misc'}})
            }
        },
        /**
         * The group link depends on the context, e.g. whether a user
         * may currently be watching the recordings view or group settings.
         */
        groupLink(groupId) {
            if ($s.admin.group && $s.admin.group._name == groupId) {
                return {name: this.$route.name}
            } else {
                return {name: this.$route.name, params: {groupId, tabId: 'misc'}}
            }
        },
        async loadGroups() {
            $s.admin.groups = await this.app.api.get('/api/groups')
        },
        async saveGroup() {
            const groupId = $s.admin.group._name
            const group = await this.$m.group.saveGroup(groupId, $s.admin.group)

            // Select the next unsaved group to speed up group creation.
            if ($s.admin.group._unsaved) {
                const nextGroupIndex = this.orderedGroups.findIndex((g) => g._unsaved)
                if (nextGroupIndex >= 0) {
                    this.toggleSelection(this.orderedGroups[nextGroupIndex]._name)
                }
            } else {
                // Reload the group, which may have been renamed.
                this.$router.push({name: 'admin-groups-settings', params: {groupId: group._name, tabId: 'misc'}})
            }
        },
        async toggleMarkDelete() {
            $s.admin.group._delete = !$s.admin.group._delete
            for (let group of $s.admin.groups) {
                if (group._name == $s.admin.group._name) {
                    group._delete = $s.admin.group._delete
                }
            }

            const similarStateGroups = this.orderedGroups.filter((i) => i._delete !== $s.admin.group._delete)
            if (similarStateGroups.length) {
                this.toggleSelection(similarStateGroups[0]._name)
            }
        },
        toggleSelection(groupId) {
            this.$router.push({name: this.$route.name, params: {groupId}})
        },
    },
    render() {
        if (!($s.admin.authenticated && $s.admin.permission)) return null
        return (
            <section class="c-admin-groups-context presence">
                <div class="actions">
                    <button class="btn" disabled={!$s.admin.group} onClick={this.toggleMarkDelete}>
                        <Icon
                            v-tip={{
                                content: $s.admin.group && $s.admin.group._delete ? $tc('group.action.delete_mark_undo') : $tc('group.action.delete_mark'),
                            }}
                            class="item-icon icon-d"
                            name="Minus"
                        />
                    </button>
                    <button class="btn">
                        <Icon
                            v-tip={{content: $t('group.action.add')}} class="item-icon icon-d"
                            name="Plus"
                            onClick={this.addGroup}
                        />
                    </button>
                    <button class="btn" disabled={!this.deletionGroups.length} onClick={this.deleteGroups}>
                        <Icon
                            v-tip={{content: `${$tc('group.action.delete_confirm', {amount: deletionGroups.length})}`}}
                            class="icon-d"
                            name="Trash"
                        />
                    </button>
                    <button class="btn" disabled={!$s.admin.group} onClick={this.saveGroup}>
                        <Icon v-tip={{content: $t('group.action.save')}} class="icon-d" name="Save" />
                    </button>
                </div>

                {this.orderedGroups.map((group) => 
                    <RouterLink
                        class={classes('group item', {active: this.$route.params.groupId === group._name})}
                        to={this.groupLink(group._name)}
                    >
                        <Icon
                            v-tip={{content: `${$t('group.name')} ${group._name}`}}
                            class={classes('item-icon icon-d', {
                                delete: group._delete,
                                unsaved: group._unsaved,
                            })}
                            name={group._delete ? 'Trash' : 'Group'}
                        />
            
                        <div class="flex-column">
                            <div class="name">
                                {group._name}
                            </div>
                            <div class="item-properties">
                                <Icon
                                    class="icon-xs"
                                    name={group.public ? 'Eye' : 'EyeClosed'}
                                />
                                <Icon
                                    class="icon-xs"
                                    name={group.locked ? 'Lock' : 'Unlock'}
                                />
                            </div>
                        </div>
                    </RouterLink>,
                )}

                {!this.orderedGroups.length && <div class="group item no-presence">
                    <Icon class="item-icon icon-d" name="Group" />
                    <div class="name">
                        {$t('group.no_groups')}
                    </div>
                </div>}
            </section>
        )
    },
    unmounted() {
        $s.admin.group = null
    },
})

