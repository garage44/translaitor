import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    computed: {
        deletionUsers() {
            return $s.admin.users.filter((i) => i._delete)
        },
        orderedUsers() {
            const users = $s.admin.users
                .filter((g) => g.admin).concat($s.admin.users.filter((g) => !g.admin))
            return users.sort((a, b) => {
                if ( a.name < b.name) return -1
                if ( a.name > b.name) return 1
                return 0
            })
        },
    },
    data() {
        return {
            selected: null,
        }
    },
    methods: {
        async addUser() {
            const user = await this.app.api.get('/api/users/template')
            $s.admin.users.push(user)
            this.toggleSelection(user.id)
        },
        async deleteUsers() {
            this.app.notifier.notify({level: 'info', message: `deleting ${this.deletionUsers.length} users`})
            const deleteRequests = []
            for (const user of this.deletionUsers) {
                $s.admin.users.splice($s.admin.users.findIndex((i) => i.id === user.id), 1)
                if (!user._unsaved) {
                    deleteRequests.push(fetch(`/api/users/${user.id}/delete`))
                }
            }

            await Promise.all(deleteRequests)

            if (this.orderedUsers.length) {
                const userId = this.orderedUsers[0].id
                this.$router.push({name: 'admin-users-settings', params: {tabId: 'misc',userId}})
            }
        },
        async loadUsers() {
            $s.admin.users = await this.app.api.get('/api/users')
        },
        async saveUser() {
            await this.$m.user.saveUser($s.admin.user.id, $s.admin.user)
            // Select the next unsaved user, when this user was unsaved to allow rapid user creation.
            if ($s.admin.user._unsaved) {
                const nextUnsavedUserIndex = this.orderedUsers.findIndex((i) => i._unsaved)
                if (nextUnsavedUserIndex >= 0) {
                    this.toggleSelection(this.orderedUsers[nextUnsavedUserIndex].id)
                }
            }
        },
        async toggleMarkDelete() {
            $s.admin.user._delete = !$s.admin.user._delete
            for (let user of $s.admin.users) {
                if (user.name == $s.admin.user.name) {
                    user._delete = $s.admin.user._delete
                }
            }

            const similarStateUsers = this.orderedUsers.filter((i) => i._delete !== $s.admin.user._delete)
            if (similarStateUsers.length) {
                this.toggleSelection(similarStateUsers[0].id)
            }
        },
        toggleSelection(userId) {
            this.$router.push({name: this.$route.name, params: {userId}})
        },
        userLink(userId) {
            if ($s.admin.user && $s.admin.user.id == userId) {
                return {name: 'admin-users'}
            } else {
                return {name: 'admin-users-settings', params: {userId}}
            }
        },
    },
    async mounted() {
        if ($s.admin.authenticated && $s.admin.permission) {
            this.loadUsers()
        }
    },
    render() {
        return (
            <section class={classes('c-admin-users-context presence', {
                collapsed: $s.panels.context.collapsed,
            })}>
                <div class="actions">
                    <button class="btn" disabled={!$s.admin.user} onClick={this.toggleMarkDelete}>
                        <Icon
                            v-tip={{
                                content: $s.admin.user && $s.admin.user._delete ? $t('user.action.delete_mark_undo') : $t('user.action.delete_mark'),
                            }}
                            class="item-icon icon-d"
                            name="Minus"
                        />
                    </button>
                    <button class="btn">
                        <Icon
                            v-tip={{content: $t('user.action.add')}} 
                            class="item-icon icon-d"
                            name="Plus"
                            onClick="addUser"
                        />
                    </button>
                    <button class="btn" disabled={!this.deletionUsers.length} onClick={this.deleteUsers}>
                        <Icon
                            v-tip={{content: `${$t('user.action.delete_confirm', {amount: this.deletionUsers.length})}`}}
                            class="icon-d"
                            name="Trash"
                        />
                    </button>
                    <button class="btn" disabled={!$s.admin.user} onClick="saveUser">
                        <Icon v-tip={{content: $t('user.action.save')}} class="icon-d" name="Save" />
                    </button>
                </div>
                {this.orderedUsers.map((user) => 
                    <RouterLink
                        class={classes('user item', {
                            active: parseInt(this.$route.params.userId) === user.id,
                        })}
                        to={this.userLink(user.id)}
                    >
                        <Icon
                            v-tip={{content: `${$t('user.name')} ${user.name}`}}
                            class={classes('item-icon icon-d', {delete: user._delete, unsaved: user._unsaved})}
                            name={user._delete ? 'Trash' : 'User'}
                        />
            
                        <div class="name">
                            {user.name}
                        </div>
                    </RouterLink>,
                )}
            </section>

        )
    },
    watch: {
        '$s.admin.authenticated': async function(authenticated) {
            if (authenticated && $s.admin.permission) this.loadUsers()
        },
    },
})
