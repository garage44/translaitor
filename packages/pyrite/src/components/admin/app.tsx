import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import Controls from './controls/controls'
import GroupsContext from './context/context-groups'
import {Notifications, PanelContext} from '@/components/elements'
import UsersContext from './context/context-users'
import {h} from 'vue'
import {RouterView} from 'vue-router'
import {$s} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            version: process.env.PYR_VERSION,
        }
    },
    mounted() {   
        const themeColor = getComputedStyle(document.querySelector('.app')).getPropertyValue('--grey-4')
        document.querySelector('meta[name="theme-color"]').content = themeColor
    },
    render() {
        return (
            <div class={classes('c-admin-app app', `theme-${$s.theme.id}`)}>
                <PanelContext>
                    {(() => {
                        if (this.$route.name.startsWith('admin-users')) {
                            return <UsersContext />
                        } 
                        return <GroupsContext />
                    })}
                </PanelContext>
                <Controls />
                <RouterView />
                <Notifications />
            </div>
        )
    },
})

