import {defineComponent} from '@/lib/component'
import {required} from '@vuelidate/validators'
import useVuelidate from '@vuelidate/core'
import {Button, FieldText, Icon} from '@/components/elements'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export const Login = defineComponent({
    data() {
        return {
            vuelidateExternalResults: {
                user: {
                    password: [],

                    username: [],
                },
            },
        }
    },
    methods: {
        async login() {
            this.vuelidateExternalResults.user.username = []
            this.vuelidateExternalResults.user.password = []

            const context = await this.app.api.post('/api/login', {
                password: this.user.password,
                username: this.user.username,
            })

            Object.assign($s.admin, context)

            if (!context.authenticated || (context.authenticated && !context.permission)) {
                let message

                if (!context.authenticated) {
                    message = $t('user.login.credentials_invalid')
                } else if (context.authenticated && !context.permission) {
                    message = $t('user.login.no_permission')
                }
                this.app.notifier.notify({level: 'error', message})
                this.vuelidateExternalResults.user.username = [message]
                this.vuelidateExternalResults.user.password = [message]

                this.v$.$validate()
            } else {
                this.app.notifier.notify({level: 'info', message: $t('user.login.succesful')})
                this.$router.push({name: 'admin-users'})
            }
        },
    },
    render() {
        return (
            <div
                class="c-admin-login content"
                onKeypress={(e) => {if (e.key === 'Enter') this.login()}}
            >
                <header>
                    <div class="notice" />
                    <div class="title">
                        <span>{$t('user.login.management')}</span>
                        <Icon class="item-icon icon-d" name="Dashboard" />
                    </div>
                </header>
                <div class="panels">
                    <section>
                        <FieldText
                            modelValue={$s.user.username}
                            autocomplete="username"
                            autofocus={true}
                            label={$t('user.username')}
                            name="username"
                            onInput={(e)=> {
                                $s.user.username = `${$s.user.username}${e.data}`

                            }}
                            placeholder={$t('user.username_placeholder')}
                            validation={this.v$.user.username}
                        />

                        <FieldText
                            modelValue={this.v$.user.password.$model}
                            autocomplete="password"
                            label={$t('user.password')}
                            name="password"
                            placeholder={$t('user.password_placeholder')}
                            type="password"
                            validation={this.v$.user.password}
                        />
                    </section>
                    <div class="actions">
                        <Button
                            icon="Login"
                            tip={$t('user.login.as_admin')}
                            variant="menu"
                            onClick={() => this.login()}
                        />
                    </div>
                </div>
            </div>
        )
    },
    setup() {
        return {v$: useVuelidate()}
    },
    validations() {
        return  {
            user: {
                password: {required},
                username: {required},
            },
        }
    },
})
