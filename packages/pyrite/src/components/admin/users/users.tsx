import {Splash} from '@/components/elements'
import {RouterView} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'
/**
 * This is a container component that handles keeping
 * track of the current user, so its child components
 * don't have to.
 */
export default {
    components: {Splash},
    async beforeMount() {
        await this.loadUsers()
        if (this.userId) {
            this.loadUser(this.userId)
        }
    },
    props: {
        userId: {
            default: () => null,
            type: String,
        },
    },
    methods: {
        async loadUser(userId) {
            this.app.logger.debug(`load user ${userId}`)
            const user = $s.admin.users.find((i) => i.id === userId)
            if (user && (user._unsaved || user._delete)) {
                $s.admin.user = user
            } else {
                $s.admin.user = await this.app.api.get(`/api/users/${encodeURIComponent(userId)}`)
            }
        },
        async loadUsers() {
            $s.admin.users = await this.app.api.get('/api/users')
        },
    },
    render() {
        return (
            <span>
                {(() => {
                    if ($s.admin.user) {
                        return <RouterView />
                    } else {
                        return <Splash instruction={$t('user.action.select')} />
                    }
                })()}
            </span>
        )
    },
    watch: {
        userId(userId) {
            if (!userId) {
                $s.admin.user = null
                return
            }
            this.loadUser(userId)
        },
    },
}

