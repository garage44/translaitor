import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'
import {FieldCheckbox,FieldText} from '@/components/elements'

export default defineComponent({
    render() {
        return (
            <section class="c-users-settings-misc tab-content active">
                <FieldText
                    v-model={$s.admin.user.name}
                    label={$t('user.settings.misc.username_label')}
                    placeholder="..."
                />
                <FieldText
                    v-model={$s.admin.user.password}
                    label={$t('user.settings.misc.password_label')}
                    placeholder="..."
                />
                <FieldCheckbox
                    v-model={$s.admin.user.admin}
                    help={$t('user.settings.misc.role_admin_help')}
                    label={$t('user.settings.misc.role_admin_label')}
                />
            </section>
        )
    },
})
