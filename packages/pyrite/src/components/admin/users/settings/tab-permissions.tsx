import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {h} from 'vue'
import {$s} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            // (!) Config directives; not the same as permissions from $s.permissions
            categories: ['op', 'presenter', 'other'],
        }
    },
    methods: {
        async loadGroups() {
            $s.admin.groups = await this.app.api.get('/api/groups')
        },
        toggleCategory(category) {
            let allSelected = true
            const groups = []
            for (const group of $s.admin.groups) {
                groups.push(group._name)
                if (!$s.admin.user.groups[category].includes(group._name)) {
                    allSelected = false
                }
            }

            if (allSelected) {
                $s.admin.user.groups[category].splice(0, $s.admin.user.groups[category].length)
            } else {
                $s.admin.user.groups[category].splice(0, $s.admin.user.groups[category].length, ...groups)
            }
        },
        toggleGroup(groupName) {
            const allSelected = this.categories.filter((c) => $s.admin.user.groups[c].includes(groupName)).length === 3

            for (const category of this.categories) {
                if (allSelected) {
                    $s.admin.user.groups[category].splice($s.admin.user.groups[category].indexOf(groupName), 1)
                } else {
                    if (!$s.admin.user.groups[category].includes(groupName)) {
                        $s.admin.user.groups[category].push(groupName)
                    }
                }
            }
        },
    },
    async mounted() {
        if ($s.admin.authenticated && $s.admin.permission) {
            this.loadGroups()
        }
    },
    render() {
        return (
            <section class="c-settings-tab-permissions tab-content permissions active">
                <div class="permission-group">
                    <div class="group-name" />
                    <div class="categories">
                        <div class="category" onClick="toggleCategory('op')">
                            <Icon v-tip="{content: $t('user.settings.permission.operator')}" class="icon-d" name="Operator" />
                        </div>
                        <div class="category" onClick="toggleCategory('presenter')">
                            <Icon v-tip="{content: $t('user.settings.permission.presenter')}" class="icon-d" name="Present" />
                        </div>
                        <div class="category" onClick="toggleCategory('other')">
                            <Icon v-tip="{content: $t('user.settings.permission.misc')}" class="icon-d" name="OtherPermissions" />
                        </div>
                    </div>
                </div>

                {$s.admin.groups.map((group) => {
                    return <div class="permission-group item">
                        <div class="group-name" onClick="toggleGroup(group._name)">
                            {group._name}
                        </div>
            
                        <div class="categories">
                            {this.categories.map((category) => {
                                return <div class="category">
                                    <input
                                        id={`${group._name}`}
                                        v-model={$s.admin.user.groups[category]}
                                        type="checkbox"
                                        value={group._name}
                                    />
                                </div>
                            })}
                        </div>
                    </div>
                })}       
            </section>
        )
    },
})
