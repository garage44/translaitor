import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import TabMisc from './tab-misc'
import TabPermissions from './tab-permissions'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    components: {TabMisc, TabPermissions},
    computed: {
        tabId() {
            return this.$route.query.tab || 'misc'
        },
    },
    methods: {
        routeSettings(tabId) {
            return {
                params: {userId: $s.admin.user.id},
                query: {tab: tabId},
                to: 'admin-users-settings',
            }
        },
        async saveUser() {
            await this.$m.user.saveUser(this.userId, $s.admin.user)
        },
    },
    props: {
        userId: {
            default: () => null,
            required: false,
            type: String,
        },
    },
    render() {
        if (!$s.admin.user) return
        return (
            <div class="c-admin-user content">
                <header>
                    <div class="notice" />
                    <div class="title">
                        <span>{$s.admin.user.name}</span>
                        <Icon class="icon icon-regular" name="User" />
                    </div>
                </header>
        
                <ul class="tabs">
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu', {active: this.tabId === 'misc'})}
                        to={this.routeSettings('misc')}
                    >
                        <Icon v-tip="{content: $t('user.settings.misc.name')}" class="icon-d" name="Pirate" />
                    </RouterLink>
                    <RouterLink
                        active-class="active-group"
                        class={classes('btn btn-menu tab', {
                            active: this.tabId === 'permissions', 
                            disabled: $s.admin.groups.length === 0,
                        })}
                        to={$s.admin.groups.length > 0 ? this.routeSettings('permissions') : this.$router.currentRoute.value.href}
                    >
                        <Icon v-tip="{content: $t('user.settings.permission.name')}" class="icon-d" name="Operator" />
                    </RouterLink>
                </ul>
        
                <div class="tabs-content">
                    {this.tabId === 'misc' && <TabMisc />}
                    {this.tabId === 'permissions' && <TabPermissions />}
                       
                    <div class="actions">
                        <button class="btn btn-menu" onClick={this.saveUser}>
                            <Icon v-tip={{content: $t('user.action.save')}} class="icon-d" name="Save" />
                        </button>
                    </div>
                </div>
            </div>
        )
    },
})
