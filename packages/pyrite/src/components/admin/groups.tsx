import {defineComponent} from '@/lib/component'
import {RouterView} from 'vue-router'
import {Splash} from '@/components/elements'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

/**
 * This is a container component that handles keeping
 * track of the current group, so its child components
 * don't have to.
 */
export default defineComponent({
    async beforeMount() {
        if (this.groupId) {
            this.loadGroup(this.groupId)
        }
    },
    props: {
        groupId: {
            default: () => null,
            required: false,
            type: String,
        },
    },
    methods: {
        async loadGroup(groupId) {
            this.app.logger.debug(`load group ${groupId}`)
            let group = $s.admin.groups.find((i) => i._name === groupId)
            if (group && group._unsaved) {
                $s.admin.group = group
            } else {
                const apiGroup = await this.app.api.get(`/api/groups/${encodeURIComponent(groupId)}`)
                if (group) {
                    // Don't update internal state properties.
                    for (const key of Object.keys(group)) {
                        if (!key.startsWith('_')) group[key] = apiGroup[key]
                    }
                } else {
                    group = apiGroup
                }

            }
            $s.admin.group = group
        },
    },
    render() {
        return (
            <span>
                {(() => {
                    if ($s.admin.group) return <RouterView />
                    return <Splash instruction={$t('group.action.select')} />
                })()}
            </span>
        )
    },
    watch: {
        groupId(groupId) {
            if (!groupId) {
                $s.admin.group = null
                return
            }
            this.loadGroup(groupId)
        },
    },
})
