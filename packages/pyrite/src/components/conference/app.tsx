import {classes} from '@/lib/utils'
import ConferenceControls from './controls/controls-main'
import {defineComponent} from '@/lib/component'
import {GroupControls} from './controls/controls-group'
import GroupsContext from './context/context-groups'
import {$s} from '@/lib/main'
import {Notifications} from '@/components/elements'
import PanelChat from './chat/panel-chat'
import {PanelContext} from '@/components/elements'
import {RouterView} from 'vue-router'
import UsersContext from './context/context-users'
import {h} from 'vue'
import {$s} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            chatToggle: false,
        }
    },
    methods: {
        async closeChat(el, done) {
            el.style.width = `${$s.chat.width}px`
            this.chatToggle = true
            this.app.animate({
                duration: 350,
                from: $s.chat.width,
                onFinish: () => {
                    this.chatToggle = false
                    done()
                },
                onUpdate: v => {
                    el.style.width = `${Math.floor(v)}px`
                },
                to: 0,
            })
        },
        async openChat(el, done) {
            el.style.width = '0px'
            this.chatToggle = true
            this.app.animate({
                duration: 350,
                from: 0,
                onFinish: () => {
                    this.chatToggle = false
                    done()
                },
                onUpdate: v => {
                    el.style.width = `${Math.floor(v)}px`
                },
                to: $s.chat.width,
            })
        },
    },
    async mounted() {
        const themeColor = getComputedStyle(document.querySelector('.app')).getPropertyValue('--grey-4')
        document.querySelector('meta[name="theme-color"]').content = themeColor

        if (!$s.chat.emoji.list.length) {
            this.app.logger.info('retrieving initial emoji list')
            $s.chat.emoji.list = JSON.parse(await this.app.api.get('/api/chat/emoji'))
            this.app.store.save()
        }
        for (const emoji of $s.chat.emoji.list) {
            this.$m.chat.emojiLookup.add(emoji.codePointAt())
        }
    },
    render() {
        return (
            <div class={classes('c-conference-app app', {
                connected: $s.group.connected,
                'panel-chat-collapsed': $s.panels.chat.collapsed,
                'panel-chat-toggle': this.chatToggle,
                'panel-context-collapsed': $s.panels.context.collapsed,
                [`theme-${$s.theme.id}`]: true,
            })}>

                <PanelContext>
                    {(() => {
                        if ($s.group.connected) return <UsersContext />
                        else return <GroupsContext />
                    })}
                </PanelContext>

                <ConferenceControls />
                <RouterView />

                {$s.group.connected && !$s.panels.chat.collapsed && (
                    <PanelChat ref="chat" />
                )}

                {$s.group.connected && <GroupControls/>}
                <Notifications />
            </div>
        )
    },
})
