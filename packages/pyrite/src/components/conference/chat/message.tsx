import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$t, app} from '@/lib/main'

const urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/ig

export default defineComponent({
    computed: {
        messageModel() {
            let messageData = []
            let textBlock = {type: 'text', value: ''}

            // Spread takes care of properly separating most special symbols
            // with multiple characters(e.g. emoji characters)
            let _message = [...this.message.message]
            for (const char of _message) {
                if (app.$m.chat.emojiLookup.has(char.codePointAt(0))) {
                    if (textBlock.value.length) {
                        messageData.push(textBlock)
                        textBlock = {type: 'text', value: ''}
                    }

                    messageData.push({type: 'emoji', value: char})
                } else {
                    textBlock.value = textBlock.value + char
                }
            }
            // Flush blocks.
            if (textBlock.value.length) {
                messageData.push(textBlock)
            }

            for (const [index, block] of messageData.entries()) {
                if (block.type !== 'text') continue
                const nodes = block.value.split(urlRegex).filter((i) => (!['http', 'https'].includes(i)))
                const replaceBlocks = []
                for (const node of nodes) {
                    if (node.match(urlRegex)) {
                        replaceBlocks.push({type: 'url', value: node})
                    } else {
                        replaceBlocks.push({type: 'text', value: node})
                    }
                }
                messageData.splice(index, 1, replaceBlocks)
            }

            return messageData.flat()
        },
    },
    methods: {
        formatTime(ts) {
            const date = new Date(ts)
            return date.toLocaleTimeString()
        },
    },
    props: {
        message: {
            type: Object,
            required: true,
        },
    },
    render() {
        return (
            <div class={classes('message', {command: !this.message.nick, [this.message.kind]: true})}>
                {this.message.kind === 'me' && (
                    <div>
                        <div class="text">
                            {this.message.nick} {$t(this.message.message)}...
                        </div>
                        <div class="time">
                            {this.formatTime(this.message.time)}
                        </div>
                    </div>
                )}

                {this.message.kind !== 'me' && [
                    this.message.nick ? <header>
                        <div class="author">
                            {this.message.nick}
                        </div><div class="time">
                            {this.formatTime(this.message.time)}
                        </div>
                    </header> : null,
                    <section>
                        {this.messageModel.map((msgModel) => {
                            <div>
                                {msgModel.type === 'text' && <span class="text">{this.msgModel.value}</span>}
                                {msgModel.type === 'emoji' && <span class="emoji">{this.msgModel.value}</span>}
                                {msgModel.type === 'url' && <a href={msgModel.value} target="_blank" class="url">
                                    {this.msgModel.value}
                                </a>}
                            </div>
                        })}
                    </section>
                ]}
            </div>
        )
    },
})
