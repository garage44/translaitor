import ChatMessage from './message'
import {classes} from '@/lib/utils'
import {nextTick} from 'vue'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import Emoji from './emoji'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    beforeUnmount() {
        this.resizeObserver.disconnect()
    },
    components: {ChatMessage, Emoji},
    computed: {
        channelMessages() {
            if ($s.chat.channels[$s.chat.channel]) {
                return $s.chat.channels[$s.chat.channel].messages
            }
            return []
        },
        formattedMessage() {
            return $s.chat.message.trim()
        },
        sortedMessages() {
            if ($s.chat.channels[$s.chat.channel]) {
                const messages = $s.chat.channels[$s.chat.channel].messages
                return messages.sort((a, b) => a.time - b.time)
            }
            return []
        },
    },
    methods: {
        addEmoji(e, emoji) {
            const message = [...$s.chat.message]
            const caretPosition = this.$refs.chatInput.selectionStart
            if (caretPosition >= 0) {
                message.splice(caretPosition, 0, emoji)
            }
            $s.chat.message = message.join('')

            if (!e.ctrlKey) {
                $s.chat.emoji.active = false
            }
        },
        async sendMessage(e) {
            if (e instanceof KeyboardEvent) {
                // ctrl/shift/meta +enter is next line.
                if (!(e.key === 'Enter' && !e.ctrlKey && !e.shiftKey && !e.metaKey)) {
                    $s.chat.message += '\r\n'
                    return false
                }
            }

            this.$m.chat.sendMessage(this.formattedMessage)
            $s.chat.message = ''
            $s.chat.emoji.active = false
        },
    },
    mounted() {
        // Keep track of the user-set width of the chat-window, so
        // we can restore it after toggling the chat window.
        this.resizeObserver = new ResizeObserver(async() => {
            $s.chat.width = parseInt(this.$refs.view.style.width.replace('px', ''))
        })

        this.resizeObserver.observe(this.$refs.view)
        this.$refs.messages.scrollTop = this.$refs.messages.scrollHeight
    },
    render() {
        return (
            <div ref="view" class={classes('c-panel-chat', {[$s.env.layout]: true})}>
                {$s.chat.emoji.active && <Emoji onselect={this.addEmoji} />}
                
                <div ref="messages" class="messages scroller">
                    {this.sortedMessages.map((message) => {
                        return <ChatMessage message={message} />
                    })}
                </div>
                
                {!$s.chat.emoji.active && (
                    <div class="chat-channels">
                        {$s.chat.channels.map((channel, key) => {
                            return (
                                <div class={classes('chat-channel', {active: channel.id === $s.chat.channel})}
                                    onClick={this.$m.chat.selectChannel(channel)}
                                >
                                    <div class="channel-name">
                                        <Icon class="icon icon-s" icon-props={{unread: channel.unread}} name="Chat" />
                                    </div>
                                
                                    <span>{key === 'main' ?  $t('chat.general') : channel.name}</span>
                                    {channel.id !== 'main' && <button 
                                        class="btn btn-icon btn-close" 
                                        onClickStop={app.$m.chat.closeChannel(channel)}
                                    >
                                        <Icon class="icon icon-xs" name="Close" />
                                    </button>}
                                </div>
                            )
                        })}
                    </div>
                )}
        
                <div class="send">
                    <textarea
                        ref="chatInput"
                        v-model={$s.chat.message}
                        autofocus="true"
                        placeholder={$t('chat.type_help')}
                        onKeydownEnter={this.$event.preventDefault()}
                        onKeyupEnter={this.sendMessage}
                    />
                    <button
                        class="btn btn-menu"
                        disabled={this.formattedMessage === ''}
                        onClick={this.sendMessage}
                    >
                        <Icon v-tip={{content: $t('chat.send_message')}} class="icon icon-s" name="Send" />
                    </button>
                </div>
        
                <div class="chat-actions">
                    <button 
                        class={classes('btn btn-menu', {
                            active: $s.chat.emoji.active,
                        })}
                        onClick={$s.chat.emoji.active = !$s.chat.emoji.active}
                    >
                        😼
                    </button>
                </div>
            </div>
        )
    },
    watch: {
        channelMessages: {
            deep: true,
            async handler() {
                await nextTick()
                if (this.$refs.messages) {
                    this.$refs.messages.scrollTop = this.$refs.messages.scrollHeight
                }
            },
        },
    },
})
