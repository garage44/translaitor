import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s} from '@/lib/main'

export default defineComponent({
    props: {
        onselect: {
            required: true,
            type: Function,
        },
    },
    render() {
        return (
            <div class="c-emoji">
                {$s.chat.emoji.list.map((emoji) => {
                    <div
                        class="emoji"
                        onClick={this.onselect(this.$event, emoji)}
                    >
                        { emoji }
                    </div>
                })}

            </div>
        )
    },
})
