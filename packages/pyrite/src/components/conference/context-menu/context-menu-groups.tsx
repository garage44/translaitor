import {classes} from '@/lib/utils'
import {Button, ContextInput} from '@/components/elements'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    computed: {
        currentGroup() {
            return this.$m.group.currentGroup()
        },
    },
    data() {
        return {
            active: false,
            lock: {
                icon: () => this.currentGroup.locked ? 'Unlock' : 'Lock',
                title: () => this.currentGroup.locked ? $t('group.action.unlock') : $t('group.action.lock'),
            },
            warning: {icon: 'Megafone', title: $t('group.action.notify')},
        }
    },
    methods: {
        clearChat() {
            this.$m.sfu.connection.groupAction('clearchat')
            this.toggleMenu()
        },
        sendNotification(text) {
            this.$m.sfu.connection.userMessage('notification', null, text)
        },
        toggleLockGroup(text) {
            if (this.currentGroup.locked) {
                this.$m.sfu.connection.groupAction('unlock')
            } else {
                this.$m.sfu.connection.groupAction('lock', text)
            }
        },
        toggleMenu(e, forceState) {
            // The v-click-outside
            if (typeof forceState === 'object') {
                this.active = false
            } else {
                this.active = !this.active
            }

            // Undo input action context state when there is no text yet...
            if (!this.active && this.warning.message === '') {
                this.warning.input = false
            }
        },
    },
    render() {
        return (
            <div 
                v-click-outside="toggleMenu" 
                class={classes('c-general-context context-menu', {
                    active: this.active,
                })}>
                <Button
                    active={false}
                    icon="Menu"
                    variant="menu"
                    onClick="toggleMenu"
                />
        
                <div v-show="active" class="context-actions">
                    {$s.permissions.op && <ContextInput
                        v-model="lock"
                        revert={$s.group.locked}
                        submit={this.toggleLockGroup}
                    />}

                    {$s.permissions.op && <ContextInput
                        v-model="warning"
                        submit={this.sendNotification}
                    />}
                </div>
            </div>
        )
    },
})
