import {classes} from '@/lib/utils'
import {ContextInput, ContextSelect, FieldFile, Icon} from '@/components/elements'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    data(){
        return {
            active: false,
            file: [],
            kick: {icon: 'Kick', title: `${$t('user.action.kick', {username: this.user.username})}`},
            statusOptions: [
                {id: 'available', name: $t('user.action.set_availability.available')},
                {id: 'away', name: $t('user.action.set_availability.away')},
                {id: 'busy', name: $t('user.action.set_availability.busy')},
            ],
            warning: {icon: 'Megafone', title: $t('user.action.notify')},
        }
    },
    methods: {
        activateUserChat() {

            this.app.emit('channel', {
                action: 'switch',
                channel: {
                    id: this.user.id,
                    messages: [],
                    name: this.user.username,
                    unread: 0,
                },
                channelId: this.user.id,
            })
            $s.panels.chat.collapsed = false
            this.toggleMenu()
        },
        kickUser(text) {
            this.app.notifier.message('kicked', {dir: 'source', target: this.user.username})
            this.$m.sfu.connection.userAction('kick', this.user.id, text)
            this.toggleMenu()
        },
        muteUser() {
            this.app.notifier.message('mute', {dir: 'source', target: this.user.username})
            this.$m.sfu.connection.userMessage('mute', this.user.id, null)
            this.toggleMenu()
        },
        sendFile(user, file) {
            if (file) {
                this.$m.sfu.connection.sendFile(user.id, file)
            } else {
                $s.files.upload = []
            }
        },
        sendNotification(message) {
            this.app.notifier.message('notification', {dir: 'source', message, target: this.user.username})
            this.$m.sfu.connection.userMessage('notification', this.user.id, message)
            this.toggleMenu()
        },
        setAvailability(availability) {
            this.$m.sfu.connection.userAction('setdata', this.$m.sfu.connection.id, {availability})
        },
        toggleMenu(e, forceState) {
            // The v-click-outside
            if (typeof forceState === 'object') {
                this.active = false
                return
            }

            this.active = !this.active
        },
        toggleOperator() {
            let action
            if (this.user.permissions.op) action = 'unop'
            else action = 'op'

            this.app.notifier.message(action, {dir: 'source', target: this.user.username})
            this.$m.sfu.connection.userAction(action, this.user.id)
            this.toggleMenu()
        },
        togglePresenter() {
            let action
            if (this.user.permissions.present) action = 'unpresent'
            else action = 'present'

            this.app.notifier.message(action, {dir: 'source', target: this.user.username})
            this.$m.sfu.connection.userAction(action, this.user.id)
            this.toggleMenu()
        },
    },
    props: {
        user: {
            required: true,
            type: Object,
        },
    },
    render() {
        return (
            <div v-click-outside="toggleMenu" class={classes('c-users-context-menu context-menu', {active: this.active})}>
                <Icon class="icon icon-d" name="Menu" onClick="toggleMenu" />
                {this.active && <div class="context-actions">
                    {this.user.id !== $s.user.id && <button class="action" onClick={this.activateUserChat}>
                        <Icon class="icon icon-s" name="Chat" />
                        {`${$t('user.action.chat', {username: this.user.username})}`}
                    </button>}

                    {this.user.id !== $s.user.id && <button class="action">
                        <FieldFile
                            v-model="$s.files.upload"
                            accept="*"
                            icon="Upload"
                            label={$t('user.action.share_file.send')}
                            onFile={(f) => this.sendFile(this.user, f)}
                        />
                    </button>}
                    
                    {($s.permissions.op && this.user.id !== $s.user.id) && <ContextInput
                        v-model="warning"
                        submit={this.sendNotification}
                    />}

                    {($s.permissions.op && this.user.id !== $s.user.id) && <button class="action" onClick={this.muteUser}>
                        <Icon class="icon icon-s" name="Mic" />
                        {$t('user.action.mute_mic')}
                    </button>}

                    {($s.permissions.op && this.user.id !== $s.user.id) && <button class="action" onClick={this.toggleOperator}>
                        <Icon class="icon icon-s" name="Operator" />
                        {this.user.permissions.op ? $t('user.action.set_role.op_retract') : $t('user.action.set_role.op_assign')}
                    </button>}

                    {($s.permissions.op && this.user.id !== $s.user.id) && <button class="action" onClick={this.togglePresenter}>
                        <Icon class="icon icon-s" name="Present" />
                        {this.user.permissions.present ? $t('user.action.set_role.present_retract') : $t('user.action.set_role.present_assign')}
                    </button>}

                    {this.user.id === $s.user.id && <ContextSelect
                        v-model="$s.user.data.availability"
                        icon="User"
                        options={this.statusOptions}
                        submit={this.setAvailability}
                        title={$t(`user.action.set_availability.${$s.user.data.availability.id}`)}
                    />}

                    {(this.user.id !== $s.user.id && $s.permissions.op) && <ContextInput
                        v-model="kick"
                        required={false}
                        submit={this.kickUser}
                    />}
        
                </div>}
            </div>
        )
    },
})
