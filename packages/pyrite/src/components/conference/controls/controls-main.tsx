import {Button} from '@/components/elements'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    computed: {
        currentGroup() {
            return this.$m.group.currentGroup()
        },
        groupRoute() {
            if ($s.group.name) {
                return {name: 'conference-groups', params: {groupId: $s.group.name}}
            } else {
                return {name: 'conference-main'}
            }
        },
        settingsRoute() {
            if (!$s.group.connected) {
                return {name: 'conference-settings', params: {tabId: 'misc'}}
            }

            if (this.$router.currentRoute.value.name ===  'conference-group-settings') {
                return {groupId: $s.group.name, name: 'conference-groups-connected'}
            } else {
                return {name: 'conference-group-settings', params: {tabId: 'misc'}}
            }
        },
    },
    methods: {
        clearChat() {
            this.$m.sfu.connection.groupAction('clearchat')
        },
        muteAllUsers() {
            this.$m.sfu.connection.userMessage('mute', null, null)
            this.active = false
        },
        sendNotification(text) {
            this.$m.sfu.connection.userMessage('notification', null, text)
        },
        toggleCollapse() {
            $s.panels.context.collapsed = !$s.panels.context.collapsed
            this.app.store.save()
        },
        toggleLockGroup(text) {
            if (this.currentGroup.locked) {
                this.$m.sfu.connection.groupAction('unlock')
            } else {
                this.$m.sfu.connection.groupAction('lock', text)
            }
        },
        toggleRecording() {
            if ($s.group.recording) {
                this.$m.sfu.connection.groupAction('unrecord')
            } else {
                this.$m.sfu.connection.groupAction('record')
            }
        },
    },
    render() {
        return (
            <nav class="c-general-controls">
                <div class="navigational-controls">
                    {!$s.group.connected && <Button
                        active={['conference-groups-connected', 'conference-groups-disconnected'].includes(this.$route.name)}
                        disabled={(!$s.group.name && !$s.group.connected)}
                        icon={this.currentGroup.locked ? 'GroupLocked' : 'Group'}
                        route={this.groupRoute}
                        tip={this.currentGroup.locked ? `${$t('group.current')} (${$t('group.locked')})` : $t('group.current')}
                        variant="menu"
                    />}
                    {$s.group.connected && $s.permissions.op && <Button 
                        context={{
                            enabled: true,
                            placeholder: $t('group.action.notify_context'),
                            submit: (text) => this.sendNotification(text),
                        }}
                        icon="Megafone"
                        tip={$t('group.action.notify')}
                        variant="menu"
                    />}

                    {$s.group.connected && $s.permissions.record && <Button
                        icon={$s.group.recording ? 'Unrecord' : 'Record'}
                        tip={$s.group.recording ? $t('group.action.stop_recording') : $t('group.action.start_recording')}
                        variant="menu"
                        onClick={this.toggleRecording()}
                    />}

                    {$s.group.connected && $s.permissions.op && <Button
                        context={{
                            enabled: !this.currentGroup.locked,
                            placeholder: $t('group.action.lock_context'),
                            submit: (text) => this.toggleLockGroup(text),
                        }}
                        icon={this.currentGroup.locked ? 'Unlock' : 'Lock'}
                        tip={this.currentGroup.locked ? $t('group.action.unlock') : $t('group.action.lock')}
                        variant="menu"
                    />}

                    {$s.group.connected && $s.permissions.op && <Button
                        icon="MicMute"
                        tip={$t('group.action.mute_participants')}
                        variant="menu"
                        onClick="muteAllUsers"
                    />}

                    {$s.group.connected && $s.permissions.op && <Button
                        icon="ChatRemove"
                        tip={$t('group.action.clear_chat')}
                        variant="menu"
                        onClick={this.clearChat}
                    />}

                    {$s.group.connected && <Button
                        class="btn-logout"
                        icon="Logout"
                        tip={$t('group.action.leave')}
                        variant="menu"
                        onClick={this.$m.sfu.disconnect}
                    />}
                </div>

                <div class="toggles">
                    <Button
                        active={this.$route.name === 'conference-settings'}
                        icon="Settings"
                        route={this.settingsRoute}
                        tip={$t('group.settings.name')}
                        variant="toggle"
                    />

                    {$s.env.layout === 'desktop' && <Button
                        active={!$s.panels.context.collapsed}
                        icon="ViewList"
                        tip={$s.panels.context.collapsed ? $t('ui.panel.expand') : $t('ui.panel.collapse')}
                        variant="toggle"
                        onClick={this.toggleCollapse}
                    />}
                </div>
            </nav>
        )
    },
})
