import {defineComponent} from '@/lib/component'
import {nextTick} from 'vue'
import {Button, FieldFile, FieldSlider} from '@/components/elements'
import {unreadMessages} from '@/models/chat.js'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export const GroupControls = defineComponent({
    computed: {
        fileMediaAccept() {
            if ($s.env.isFirefox) {
                return '.mp4'
            } else {
                // Chromium supports at least these 3 formats:
                return '.mp4,.mkv,.webm'
            }
        },
        filePlayTooltip() {
            if ($s.files.playing.length) {
                return $t('file.streaming')
            }
            let formats = []
            if ($s.env.isFirefox) {
                formats.push('.mp4')
            } else {
                formats.push('.mp4', 'webm', 'mkv')
            }
            return $t('file.stream', {formats: formats.join(',')})
        },
        unreadMessages,
    },
    data() {
        return {
            volume: {
                locked: null,
                value: 100,
            },
        }
    },
    methods: {
        toggleCam() {
            $s.devices.cam.enabled = !$s.devices.cam.enabled
            this.app.logger.debug(`switching cam stream: ${$s.devices.cam.enabled}`)
            this.$m.sfu.delUpMediaKind('camera')
            this.$m.media.getUserMedia($s.devices)
        },
        async toggleChat() {
            // Don't do a collapse animation while emoji is active; this is
            // too heavy due to the 1800+ items grid layout.
            $s.chat.emoji.active = false
            await nextTick()
            $s.panels.chat.collapsed = !$s.panels.chat.collapsed
            this.app.store.save()
        },
        toggleMicrophone() {
            let shouldRestartStream = !$s.devices.cam.enabled
            this.$m.sfu.muteMicrophone($s.devices.mic.enabled)
            if (shouldRestartStream) {
                // When both the camera is off, toggling the microphone should also restart the stream.
                // Otherwise, we would either continue to stream empty data (when both camera and mic are
                // off), or we would not send our audio stream altogether.
                this.$m.media.getUserMedia($s.devices)
            }
        },
        togglePlayFile(file) {
            if (file) {
                this.$m.sfu.addFileMedia(file)
            } else {
                $s.files.playing = []
                this.$m.sfu.delUpMediaKind('video')
            }
        },
        toggleRaiseHand() {
            this.$m.sfu.connection.userAction('setdata', this.$m.sfu.connection.id, {raisehand: !$s.user.data.raisehand})
            if (!$s.user.data.raisehand) {
                this.app.$m.sfu.connection.userMessage('raisehand')
            }
        },
        async toggleScreenshare() {
            if ($s.upMedia.screenshare.length) {
                this.app.logger.debug('turn screenshare stream off')
                this.$m.sfu.delUpMedia(this.$m.media.screenStream)
            } else {
                this.app.logger.debug('turn screenshare stream on')
                const stream = await this.$m.sfu.addShareMedia()
                this.$m.media.setScreenStream(stream)
            }
        },
    },
    render() {
        return (
            <div class="c-group-controls">
                <Button
                    active={!$s.panels.chat.collapsed}
                    icon="Chat"
                    icon-props={{unread: unreadMessages}}
                    tip={$s.panels.chat.collapsed ? $t('ui.panel_chat.expand') : $t('ui.panel_chat.collapse')}
                    variant="toggle"
                    onClick={this.toggleChat}
                />
                {$s.permissions.present && [
                    <Button
                        active={$s.devices.mic.enabled ? $s.devices.mic.enabled : null}
                        icon={$s.devices.mic.enabled ? 'Mic' : 'MicMute'}
                        tip={$s.devices.mic.enabled ? $t('group.action.mic_off') : $t('group.action.mic_on')}
                        variant="toggle"
                        onClick={this.toggleMicrophone}
                    />,
        
                    <Button
                        active={$s.devices.cam.enabled}
                        disabled={!$s.mediaReady}
                        icon="Webcam"
                        tip={$s.devices.cam.enabled ? $t('group.action.cam_off') : $t('group.action.cam_on')}
                        variant="toggle"
                        onClick={this.toggleCam}
                    />,
            
                    <Button
                        active={!!$s.upMedia.screenshare.length}
                        icon="ScreenShare"
                        tip={$s.upMedia.screenshare.length ? $t('group.action.screenshare_off') : $t('group.action.screenshare_on')}
                        variant="toggle"
                        onClick={this.toggleScreenshare}
                    />,
            
                    <Button
                        active={!!$s.upMedia.video.length}
                        variant="toggle"
                    >
                        <FieldFile
                            v-model="$s.files.playing"
                            accept={this.fileMediaAccept}
                            tooltip={this.filePlayTooltip}
                            onFile={this.togglePlayFile}
                        />
                    </Button>,
                ]}

                {$s.group.connected && 
                <Button                    
                    active={$s.user.data.raisehand}
                    icon="Hand"
                    tip={$s.user.data.raisehand ? $t('group.action.raisehand_active') : $t('group.action.raisehand')}
                    variant="toggle"
                    onClick={this.toggleRaiseHand}
                />}
        
                <Button
                    class="no-feedback"
                    tip={`${this.volume.value}% ${$t('group.audio_volume')}`}
                    variant="unset"
                >
                    <FieldSlider v-model="volume" />
                </Button>
            </div>
        )
    },
    watch: {
        '$s.devices.mic.enabled'(enabled) {
            this.$m.sfu.connection.userAction('setdata', this.$m.sfu.connection.id, {mic: enabled})
        },
        '$s.permissions.present'() {
            this.$m.media.getUserMedia($s.devices)
        },
        volume(volume) {
            for (const description of $s.streams) {
                // Only downstreams have volume control:
                if (description.direction === 'down' && !description.volume.locked) {
                    description.volume = volume
                }
            }
        },
    },
})
