import {classes} from '@/lib/utils'
import ContextMenu from '../context-menu/context-menu-users'
import {Icon} from '@/components/elements'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    components: {ContextMenu},
    computed: {
        sortedUsers() {
            const users = [...$s.users]
            users.sort(function(a, b) {
                if (!a.username || !b.username) return 0
                const aLowerName = a.username.toLowerCase()
                const bLowerName = b.username.toLowerCase()
                if (aLowerName < bLowerName) return -1
                else if (aLowerName > bLowerName) return +1
                else if (a.username < b.username) return -1
                else if (a.username > b.username) return +1
                return 0
            })
            return users
        },
    },
    methods: {
        className(user) {
            const classes = {}
            if (user.data.raisehand) {
                classes.hand = true
            }
            if (user.data.availability) {
                if (user.data.availability.id === 'away') {
                    classes.away = true
                } else if (user.data.availability.id === 'busy') {
                    classes.busy = true
                }
            }

            return classes
        },
    },
    render() {
        return (
            <section class="c-users-context presence">
                {this.sortedUsers.map((user) => {
                    return (
                        <div class="user item">
                            <Icon
                                v-tip={{content: `${$t('user.username')} ${user.username}`}}
                                class={classes('icon item-icon icon-d', this.className(user))}
                                name={user.data.raisehand ? 'Hand' : 'User'}
                            />
                
                            <div class="name">
                                <div v-if="user.username" class="username">
                                    {user.username === 'RECORDING' ? $t('user.recorder') : user.username}
                                </div>
                                <div v-else class="username">
                                    {$t('user.anonymous')}
                                </div>
                                <div v-if="$s.users[0].id === user.id" class="username">
                                    ({$t('user.you')})
                                </div>
                
                                <div class="status">
                                    {(() => {
                                        if (user.data.mic) {
                                            return <Icon class="icon icon-s" name="Mic" />
                                        }
                                        return <Icon class="icon icon-s error" name="MicMute" />
                                    })()}
                                </div>
                
                                <div class="permissions">
                                    {user.permissions.present && <span>
                                        <Icon v-tip={{content: $t('user.role.presenter')}} class="icon icon-s" name="Present" />
                                    </span>}
                                    {user.permissions.op && <span>
                                        <Icon v-tip={{content: $t('user.role.operator')}} class="icon icon-s" name="Operator" />
                                    </span>}
                                </div>
                            </div>
                            {user.username !== 'RECORDING' && <ContextMenu user={user} />}                            
                        </div>
                    )
                })}
            </section>
        )
    },
})
