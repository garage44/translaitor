import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    computed: {
        currentGroup() {
            return this.$m.group.currentGroup()
        },
        isListedGroup() {
            return !!$s.groups.find((i) => i.name === $s.group.name)
        },
    },
    methods: {
        groupLink(groupId) {
            if ($s.group && $s.group.name === groupId) {
                return {name: 'conference-splash'}
            } else {
                return {name: 'conference-groups', params: {groupId}}
            }
        },
        async pollGroups() {
            const groups = await this.app.api.get('/api/groups/public')
            if (!$s.groups.length) {
                $s.groups = groups
            } else {
                for (const group of groups) {
                    if (group.name === $s.group.name) {
                        this.currentGroup.locked = group.locked
                    }
                    const _group = $s.groups.find((g) => g.name === group.name)
                    if (_group) {
                        Object.assign(_group, {
                            clientCount: group.clientCount,
                            comment: group.comment,
                            locked: group.locked,
                        })
                    } else {
                        $s.groups.push(group)
                    }

                }
            }
        },
        setAutofocus() {
            $s.login.autofocus = true
        },
        toggleUnlisted() {
            if (!$s.group.name || this.isListedGroup) {
                $s.group.name = $t('group.unlisted')
            } else if (!this.isListedGroup) {
                $s.group.name = ''
            }
        },
        updateRoute() {
            $s.login.autofocus = false

            if ($s.group.name) {
                // Assume unlocked, when there are no public groups
                $s.group.locked = $s.groups.find((i) => i.name === $s.group.name)?.locked || false

                // Update the group route when the user sets the group name.
                this.$router.replace({name: 'conference-groups', params: {groupId: $s.group.name}})
            } else {
                // By default show the splash page when emptying the group input.
                this.$router.replace({name: 'conference-splash'})
            }
        },
    },
    async mounted() {
        this.intervalId = setInterval(this.pollGroups, 3000)
        this.pollGroups()
    },
    render() {
        return (
            <section class={classes('c-groups-context presence', {collapsed: $s.panels.context.collapsed})}>
                <div class="actions">
                    <div
                        class={classes('group item unlisted-group', {
                            active: this.$route.name !== 'conference-splash' && !this.isListedGroup,
                        })}
                        onClick={this.toggleUnlisted}
                    >
                        <Icon v-tip={{content: $t('group.join_unlisted')}} class="icon item-icon icon-d" name="Incognito" />
                        <div class="flex-column">
                            <div v-if="isListedGroup || !$s.group.name" class="name">
                                ...
                            </div>
                            <div v-else class="name">
                                {$s.group.name}
                            </div>
                        </div>
                    </div>
                </div>
                {$s.groups.map((group) => {
                    <RouterLink
                        key={group.name}
                        class={classes('group item', {active: this.currentGroup.name === group.name})}
                        to={this.groupLink(group.name)}
                        onClick={this.setAutofocus}
                    >
                        <Icon
                            v-tip={{content: `${$t('group.name')}: ${group.name} (${group.clientCount})`}}
                            class="icon item-icon icon-d"
                            name={group.locked ? 'GroupLocked' : 'Group'}
                        />
            
                        <div class="flex-column">
                            <div class="name">
                                {group.name}
                            </div>
                            <div v-if="group.description" class="item-properties">
                                {group.description}
                            </div>
                        </div>
            
                        <div class={classes('stats', {active: group.clientCount > 0})}>
                            {group.clientCount}
                            <Icon class="icon-d" name="User" />
                        </div>
                    </RouterLink>
                })}

                {!$s.groups.length && <div class="group item no-presence">
                    <Icon class="item-icon icon-d" name="Group" />
                    <div class="name">
                        {$t('group.no_groups_public')}
                    </div>
                </div>}
            </section>
        )
    },
    unmounted() {
        clearInterval(this.intervalId)
    },
    watch: {
        /**
         * Note that the behaviour is that using the custom group
         * input does NOT trigger the 'user-groups-disconnected' view,
         * while using the listed groups selection does. This is
         * intended behaviour to keep the history clean.
         */
        '$s.group.name': {
            immediate: false,
            handler() {
                this.app.logger.debug(`updating group route: ${$s.group.name}`)
                this.updateRoute()

            },
        },
    },
})

