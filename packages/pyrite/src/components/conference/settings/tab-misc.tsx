import {defineComponent} from '@/lib/component'
import {FieldSelect} from '@/components/elements'
import {nextTick} from 'vue'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            languages: [
                {id: 'de', name: 'ui.settings.misc.language.german_label'},
                {id: 'en', name: 'ui.settings.misc.language.english_label'},
                {id: 'fr', name: 'ui.settings.misc.language.french_label'},
                {id: 'nl', name: 'ui.settings.misc.language.dutch_label'},
            ],
            themes: [
                {id: 'system', name: 'ui.settings.misc.theme.system_label'},
                {id: 'light', name: 'ui.settings.misc.theme.light_label'},
                {id: 'dark', name: 'ui.settings.misc.theme.dark_label'},
            ],
        }
    },
    render() {
        return (
            <section class="tab-content active">
                <FieldSelect
                    value={$s.theme}              
                    onUpdate:modelValue={(value) => $s.theme = value} 
                    help={$t('ui.settings.misc.theme_help')}
                    label={$t('ui.settings.misc.theme_label')}
                    name="language"
                    options={this.themes}
                />
        
                <FieldSelect
                    value={$s.language}   
                    onUpdate:modelValue={(value) => $s.language = value}                  
                    help={$t('ui.settings.misc.language_help')}
                    label={$t('ui.settings.misc.language_label')}
                    name="language"
                    options={this.languages}
                />
            </section>
        )
    },
    watch: {
        async '$s.theme'() {
            await nextTick()
            const themeColor = getComputedStyle(document.querySelector('.app')).getPropertyValue('--grey-4')
            this.app.logger.info(`setting theme color to ${themeColor}`)
            document.querySelector('meta[name="theme-color"]').content = themeColor
        },
    },
})

