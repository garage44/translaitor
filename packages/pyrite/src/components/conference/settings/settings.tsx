import {defineComponent} from '@/lib/component'
import {Button, Icon} from '@/components/elements'
import TabDevices from './tab-devices'
import TabMedia from './tab-media'
import TabMisc from './tab-misc'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    computed: {
        settingsRoute() {
            if ($s.group.connected) {
                return 'conference-group-settings'
            } else {
                return 'conference-settings'
            }
        },
    },
    methods: {
        async saveSettings() {
            await this.app.setLanguage($s.language.id)
            this.app.logger.debug(`settings language to ${$s.language.id}`)
            this.app.store.save()
            this.app.notifier.notify({icon: 'Settings', level: 'info', message: $t('ui.settings.action.saved')})
        },
    },
    render() {
        return <div class="c-settings content">
            <header>
                <div class="notice" />
                <div class="title">
                    <span>{$t('ui.settings.name')}</span>
                    <Icon class="item-icon icon-regular" name="Settings" />
                </div>
            </header>
            <div class="tabs">
                <Button
                    active={this.$route.name.startsWith('admin-users')}
                    icon="Pirate"
                    route={{name: this.settingsRoute, params: {tabId: 'misc'}}}
                    tip={$t('ui.settings.misc.name')}
                    variant="menu"
                />
    
                <Button
                    active={this.$route.name.startsWith('admin-users')}
                    icon="Media"
                    route={{name: this.settingsRoute, params: {tabId: 'media'}}}
                    tip={$t('ui.settings.media.name')}
                    variant="menu"
                />
    
                <Button
                    active={this.$route.name.startsWith('admin-users')}
                    icon="Webcam"
                    route={{name: this.settingsRoute, params: {tabId: 'devices'}}}
                    tip={$t('ui.settings.devices')}
                    variant="menu"
                />
            </div>
    
            <div class="tabs-content">
                {this.$route.params.tabId === 'devices' && <TabDevices />}
                {this.$route.params.tabId === 'media' && <TabMedia />}
                {this.$route.params.tabId === 'misc' && <TabMisc />}
                
                <div class="actions">
                    <Button
                        icon="Save"
                        tip={$t('ui.settings.action.save')}
                        variant="menu"
                        onClick={this.saveSettings}
                    />
                </div>
            </div>
        </div>
    },
})
