import {FieldSelect} from '@/components/elements'
import {nextTick} from 'vue'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    data() {
        return {
            acceptOptions: [
                {id: 'nothing', name: $t('ui.settings.media.accept.nothing_label')},
                {id: 'audio', name: $t('ui.settings.media.accept.audio_label')},
                {id: 'screenshare', name: $t('ui.settings.media.accept.screenshare_label')},
                {id: 'everything', name: $t('ui.settings.media.accept.everything_label')},
            ],
            bandwidthOptions: [
                {id: 'lowest', name: $t('ui.settings.media.bandwidth.lowest_label')},
                {id: 'low', name: $t('ui.settings.media.bandwidth.low_label')},
                {id: 'normal', name: $t('ui.settings.media.bandwidth.normal_label')},
                {
                    help: $t('ui.settings.media.bandwidth.unlimited_help'),
                    id: 'unlimited',
                    name: $t('ui.settings.media.bandwidth.unlimited_label'),
                },
            ],
            resolutionOptions: [
                {id: 'default', name: $t('ui.settings.media.resolution.default_label')},
                {id: '720p', name: $t('ui.settings.media.resolution.720p_label')},
                {
                    help: $t('ui.settings.media.resolution.1080p_help'),
                    id: '1080p',
                    name: $t('ui.settings.media.resolution.1080p_label'),
                },
            ],
        }
    },
    render() {
        return (
            <section class="tab-content active">
                <FieldSelect
                    value={$s.media.accept}
                    onUpdate:modelValue={(value) => $s.media.accept = value}
                    help={$t('ui.settings.media.accept_help')}
                    label={$t('ui.settings.media.accept_label')}
                    name="request"
                    options={this.acceptOptions}
                />

                <FieldSelect
                    value={$s.devices.cam.resolution}
                    onUpdate:modelValue={(value) => $s.devices.cam.resolution = value}
                    help={$t('ui.settings.media.resolution_help')}
                    label={$t('ui.settings.media.resolution_label')}
                    name="resolution"
                    options={this.resolutionOptions}
                />

                <FieldSelect
                    value={$s.media.upstream}
                    onUpdate:modelValue={(value) => $s.media.upstream = value}
                    help={$t('ui.settings.media.bandwidth_help')}
                    label={$t('ui.settings.media.bandwidth_label')}
                    name="send"
                    options={this.bandwidthOptions}
                />
            </section>
        )
    },
    watch: {
        async '$s.theme'() {
            await nextTick()
            const themeColor = getComputedStyle(document.querySelector('.app')).getPropertyValue('--grey-4')
            this.app.logger.info(`setting theme color to ${themeColor}`)
            document.querySelector('meta[name="theme-color"]').content = themeColor
        },
    },
})
