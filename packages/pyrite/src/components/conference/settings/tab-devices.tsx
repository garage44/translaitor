import {FieldSelect, Icon, SoundMeter} from '@/components/elements'
import {required} from '@vuelidate/validators'
import Sound from '@/lib/sound.js'
import {Stream} from '@/components/conference/stream/stream'
import useVuelidate from '@vuelidate/core'
import {nextTick} from 'vue'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export default defineComponent({
    beforeUnmount() {
        if (!$s.group.connected) {
            this.$m.sfu.delLocalMedia()
        }
    },
    components: {Stream},
    data() {
        return {
            description: null,
            devices: $s.devices,
            // Keep track of test sounds that are playing.
            playing: {
                audio: false,
            },
            sound: {
                audio: {
                    file: '/audio/power-on.ogg',
                    playing: false,
                },
            },
            stream: null,
            streamId: null,
        }
    },
    methods: {
        async remountStream() {
            this.stream = await this.$m.media.getUserMedia()
            this.streamId = this.stream.id
            this.description = null
            // Give the stream time to unmount first...
            await nextTick()

            this.description = {
                direction: 'up',
                hasAudio: $s.devices.mic.enabled,
                hasVideo: $s.devices.cam.enabled,
                id: this.stream.id,
                kind: 'video',
                mirror: false,
                src: this.stream,
                volume: {
                    locked: false,
                    value: 100,
                },
            }
        },
        testSoundAudio() {
            this.soundAudio.play()
        },
    },
    async mounted() {
        await this.$m.media.queryDevices()
        this.soundAudio = new Sound(this.sound.audio)

        // Not a media stream yet? Create one for the audio settings
        if (!$s.group.connected) {
            const res = await this.$m.media.getUserMedia()
            if (!res) {
                this.app.notifier.notify({level: 'error', message: $t('device.media_error')})
                return
            }
        }
        this.stream = this.$m.media.localStream
        this.streamId = this.$m.media.localStream.id
        this.description = {
            direction: 'up',
            hasAudio: $s.devices.mic.enabled,
            hasVideo: $s.devices.cam.enabled,
            id: this.stream.id,
            kind: 'video',
            mirror: false,
            src: this.stream,
            volume: {
                locked: false,
                value: 100,
            },
        }
    },
    render() {
        return (
            <section class="c-tab-devices tab-content active">
                <div class="camera-field">
                    <FieldSelect
                        value={this.v$.devices.cam.selected.$model}
                        onUpdate:modelValue={(value) => this.v$.devices.cam.selected.$model = value}
                        help={$t('device.select_cam_help')}
                        label={$t('device.select_cam_label')}
                        name="video"
                        options={$s.devices.cam.options}
                        validation={this.v$.devices.cam.selected.id}
                    />

                    {this.description && <Stream modelValue={this.description} controls={false} />}
                    {!this.description && <div class="webcam-placeholder">
                        <Icon name="Webcam" />
                    </div>}
                </div>

                <FieldSelect
                    value={this.v$.devices.mic.selected.$model}
                    onUpdate:modelValue={(value) => this.v$.devices.mic.selected.$model = value}
                    help={$t('device.select_mic_verify_help')}
                    label={$t('device.select_mic_label')}
                    name="audio"
                    options={$s.devices.mic.options}
                    validation={this.v$.devices.mic.selected.id}
                />

                <div class="soundmeter">
                    {this.streamId && this.stream && <SoundMeter stream={this.stream} stream-id={this.streamId} />}
                </div>

                <div class="output-config">
                    {/* <!-- https://bugzilla.mozilla.org/show_bug.cgi?id=1498512 -->
                    <!-- https://bugzilla.mozilla.org/show_bug.cgi?id=1152401 --> */}
                    {$s.devices.audio.options.length && !$s.env.isFirefox && (
                        <FieldSelect
                            value={this.v$.devices.audio.selected.$model}
                            onUpdate:modelValue={(value) => this.v$.devices.audio.selected.$model = value}
                            help={$t('device.select_audio_verify_help')}
                            label={$t('device.select_audio_label')}
                            name="audio"
                            options={$s.devices.audio.options}
                            validation={this.v$.devices.audio.selected.id}
                        >
                            {/* <template #header>
                            <button class="btn" :disabled="sound.audio.playing" @click="soundAudio.play()">
                                <Icon class="icon-d" name="Play" />
                            </button>
                        </template> */}
                        </FieldSelect>
                    )}

                    {($s.env.isFirefox || !$s.devices.audio.options.length) && <div class="field">
                        <div class="label-container">
                            <label class="field-label">{$t('device.select_audio_label')}</label>
                            <button class="btn" disabled={this.sound.audio.playing} onClick={() => this.soundAudio.play()}>
                                <Icon class="icon-d" name="Play" />
                            </button>
                        </div>

                        <div class="help">
                            {$t('device.select_audio_verify_help')}
                        </div>
                    </div>}
                </div>
            </section>
        )
    },
    setup() {
        return {v$: useVuelidate()}
    },
    validations() {
        const validations = {
            devices: {
                audio: {selected: {id: {required}}},
                cam: {selected: {id: {required}}},
                mic: {selected: {id: {required}}},
            },

        }
        return validations
    },
    watch: {
        '$s.devices.cam.resolution'() {
            this.remountStream()
        },
        '$s.devices.cam.selected'() {
            this.remountStream()
        },
        '$s.devices.mic.selected'() {
            this.remountStream()
        },
    },
})
