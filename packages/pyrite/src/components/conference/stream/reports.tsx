import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$t} from '@/lib/main'

export const Reports = defineComponent({
    computed: {
        hasAudioStats() {
            if (this.description.settings && this.description.settings.audio && Object.keys(this.description.settings.audio).length) {
                return true
            }

            return false
        },
        hasVideoStats() {
            if (this.description.settings && this.description.settings.video && Object.keys(this.description.settings.video).length) {
                return true
            }

            return false
        },
    },
    data() {
        return {
            active: null,
            glnStream: null,
            stats: {},
        }
    },
    methods: {
        onDownStats() {
            this.glnStream.pc.getReceivers().forEach(r => {
                let tid = r.track && r.track.id

                const stats = tid && this.glnStream.stats[tid]
                if (stats) {
                    const filtered = {}
                    for (const [categoryName, category] of Object.entries(stats)) {
                        filtered[categoryName] = {}
                        if (categoryName === 'track') {
                            for (const [statName, stat] of Object.entries(category)) {
                                if (statName === 'timestamp') {
                                    continue
                                } else if (statName === 'totalAudioEnergy') {
                                    filtered[categoryName]['Total Audio Energy'] = Number(stat).toFixed(2)
                                } else if (statName === 'audioEnergy') {
                                    filtered[categoryName]['Audio Energy'] = Number(stat).toFixed(2)
                                }
                            }
                        }
                    }
                    this.stats = filtered

                }
            })
        },
        onUpStats() {
            this.glnStream.pc.getSenders().forEach(s => {
                let tid = s.track && s.track.id
                const stats = this.glnStream.stats[tid]

                if (stats) {
                    const filtered = {}
                    for (const [categoryName, category] of Object.entries(stats)) {
                        filtered[categoryName] = {}
                        if (categoryName === 'outbound-rtp') {
                            for (const [statName, stat] of Object.entries(category)) {
                                if (statName === 'timestamp') {
                                    continue
                                } else if (statName === 'rate') {
                                    filtered[categoryName]['Data Rate'] = `${Math.round(stat / 1000)} Kbps`
                                } else if (statName === 'bytesSent') {
                                    filtered[categoryName]['Streamed'] = `${Math.round(stat / 1000 / 1024)} Mb`
                                }
                            }
                        }
                    }

                    this.stats = filtered
                }
            })
        },
    },
    mounted() {
        this.glnStream = null
        if (this.$m.sfu.connection.up[this.description.id]) {
            this.glnStream = this.$m.sfu.connection.up[this.description.id]
            this.glnStream.onstats = this.onUpStats
        } else {
            this.glnStream = this.$m.sfu.connection.down[this.description.id]
            this.glnStream.onstats = this.onDownStats
        }

        this.glnStream.setStatsInterval(250)
    },
    props: {
        description: {
            default: () => {},
            type: Object,
        },
    },
    render() {
        return (
            <div class="c-stream-reports">
                {!this.hasAudioStats && !this.hasVideoStats && <div class="no-stats-available">
                    {$t('group.report.not_available')}
                </div>}

                {this.hasVideoStats && <div class="category">
                    <div class="title">
                        {$t('group.report.video')}
                    </div>
                    {this.description.settings.video.map((stat, statName) => {
                        return (
                            <div class="stat">
                                <div class="key">
                                    {statName}
                                </div>
                                <div class="value">
                                    {stat}
                                </div>
                            </div>
                        )
                    })}
                </div>}

                {this.hasAudioStats && <div class="category">
                    <div class="title">
                        {$t('group.report.audio')}
                    </div>
                    {this.description.settings.video.audio.map((stat, statName) => {
                        return (
                            <div class="stat">
                                <div class="key">
                                    {statName}
                                </div>
                                <div class="value">
                                    {stat}
                                </div>
                            </div>
                        )
                    })}
                </div>}

                {this.stats.map((category, categoryName) => {
                    return (
                        <div class="category">
                            <div class="title">
                                {categoryName}
                            </div>
                            {category.map((stat, statName) => {
                                return (
                                    <div class="stat">
                                        <div class="key">
                                            {statName}
                                        </div>
                                        <div class="value">
                                            {stat}
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
            </div>
        )
    },
    unmounted() {
        this.glnStream.onstats = null
    },
})
