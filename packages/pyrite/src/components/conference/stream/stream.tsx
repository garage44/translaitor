import {classes} from '@/lib/utils'
import {h} from 'vue'
import {Button, FieldSlider, SoundMeter} from '@/components/elements'
import {defineComponent,nextTick} from 'vue'
import {Icon} from '@/components/elements'
import {Reports} from './reports'
import {$s, $t} from '@/lib/main'

export const Stream = defineComponent({
    beforeUnmount() {
        this.app.logger.debug(`unmounting ${this.modelValue.direction} stream ${this.modelValue.id}`)
        if (this.$refs.media.src) {
            URL.revokeObjectURL(this.$refs.media.src)
        } else {
            this.$refs.media.srcObject = null
        }
    },
    components: {Reports},
    computed: {
        audioEnabled() {
            return !!(this.modelValue.hasAudio && this.stream && this.stream.getAudioTracks().length)
        },
        fullscreenEnabled() {
            if (this.$refs.media) {
                return this.$refs.media.requestFullscreen
            }
            return false
        },
        hasSettings() {
            if (!this.modelValue?.settings) return false

            // Firefox MediaStream settings are empty.
            // See https://bugzilla.mozilla.org/show_bug.cgi?id=1537986
            return (
                Object.keys(this.modelValue.settings.audio).length ||
                Object.keys(this.modelValue.settings.video).length
            )
        },
        pipEnabled() {
            if (this.$refs.media) {
                return this.$refs.media.requestPictureInPicture
            }
            return false
        },
        volume: {
            get() { return this.modelValue.volume },
            set(volume) { this.$emit('update:modelValue', {...this.modelValue, volume}) },
        },
    },
    data() {
        return {
            bar: {
                active: false,
            },
            media: null,
            mediaFailed: false,
            muted: false,
            pip: {
                active: false,
                enabled: false,
            },
            stats: {
                visible: false,
            },
            stream: null,
            type: 'video',
        }
    },
    emits: ['update:modelValue'],
    methods: {
        async loadSettings() {
            this.app.logger.debug('loading stream settings')
            const settings = {audio: {}, video: {}}

            const audioTracks = this.stream.getAudioTracks()

            if (audioTracks.length) {
                settings.audio = audioTracks[0].getSettings()
            }

            const videoTracks = this.stream.getVideoTracks()
            if (videoTracks.length) {
                settings.video = videoTracks[0].getSettings()
            }

            await nextTick()
            this.$emit('update:modelValue', {...this.modelValue, settings})
        },
        async mountDownstream() {
            this.glnStream = this.$m.sfu.connection.down[this.modelValue.id]

            if (!this.glnStream) {
                this.app.logger.debug(`no sfu stream on mounting stream ${this.modelValue.id}`)
                return
            } else {
                this.app.logger.debug(`mount downstream ${this.modelValue.id}`)
            }

            this.stream = this.glnStream.stream

            // No need for further setup; this is an existing stream.
            if (this.$m.sfu.connection.down[this.modelValue.id].stream) {
                this.$refs.media.srcObject = this.$m.sfu.connection.down[this.modelValue.id].stream
                await this.playStream()
                return
            }

            this.glnStream.ondowntrack = (track) => {
                if (!this.stream) {
                    this.stream = this.glnStream.stream
                }

                this.app.logger.debug(`downstream ondowntrack/${this.glnStream.id}`)
                // An incoming audio-track; enable volume controls.
                if (track.kind === 'audio') {
                    this.app.logger.debug(`stream ondowntrack - enable audio controls`)
                    this.$emit('update:modelValue', {...this.modelValue, hasAudio: true})
                } else if (track.kind === 'video') {
                    this.$emit('update:modelValue', {...this.modelValue, hasVideo: true})
                }
            }

            this.glnStream.onclose = () => {
                this.$m.sfu.delMedia(this.glnStream.id)
            }

            this.glnStream.onstatus = async(status) => {
                if (['connected', 'completed'].includes(status)) {

                    this.$refs.media.srcObject = this.stream
                    // Firefox doesn't have a working setSinkId
                    // See https://bugzilla.mozilla.org/show_bug.cgi?id=1498512
                    if (this.audioEnabled && this.$refs.media.setSinkId) {
                        this.app.logger.debug(`set stream sink: ${$s.devices.audio.selected.id}`)
                        this.$refs.media.setSinkId($s.devices.audio.selected.id)
                    }

                    await this.playStream()
                }
            }
        },
        async mountUpstream() {
            // Mute local streams, so people don't hear themselves talk.
            if (!this.muted) {
                this.toggleMuteVolume()
            }
            this.app.logger.debug(`mount upstream ${this.modelValue.id}`)

            if (!this.modelValue.src) {
                // Local media stream from a device.
                this.glnStream = this.$m.sfu.connection.up[this.modelValue.id]
                this.stream = this.glnStream.stream
                this.$refs.media.srcObject = this.stream
                await this.playStream()
            } else {
                // Local media stream playing from a file...
                if (this.modelValue.src instanceof File) {
                    const url = URL.createObjectURL(this.modelValue.src)
                    this.$refs.media.src = url

                    if (this.$refs.media.captureStream) {
                        this.stream = this.$refs.media.captureStream()
                    } else if (this.$refs.media.mozCaptureStream) {
                        this.stream = this.$refs.media.mozCaptureStream()
                    }

                    this.glnStream = this.$m.sfu.connection.up[this.modelValue.id]
                    this.glnStream.userdata.play = true

                    this.stream.onaddtrack = (e) => {
                        const track = e.track

                        if (track.kind === 'audio') {
                            this.$emit('update:modelValue', {...this.modelValue, hasAudio: true})
                        } else if (track.kind === 'video') {
                            this.$emit('update:modelValue', {...this.modelValue, hasVideo: true})
                        }

                        this.glnStream.pc.addTrack(track, this.stream)
                    }

                    // Previously handled by `glnStream.onclose`, but that event doesn't seem to be
                    // triggered here. TODO: Checkout later if glnStream can be used instead.
                    this.stream.onremovetrack = () => {
                        if (this.$refs.media && this.$refs.media.src) {
                            this.$m.sfu.delUpMedia(this.glnStream)
                            $s.files.playing = []
                        }
                    }

                    this.glnStream.onstatus = async(status) => {
                        if (status === 'connected') {
                            await this.loadSettings()
                        }
                    }

                } else if (this.modelValue.src instanceof MediaStream) {
                    // Local MediaStream (not part of Galene); e.g. Webcam test
                    this.stream = this.modelValue.src
                    this.$refs.media.srcObject = this.stream
                    await this.playStream()
                } else {
                    throw new Error('invalid Stream source type')
                }
            }

            // A local stream that's not networked (e.g. cam preview in settings)
            if (!this.glnStream) return

            this.glnStream.stream = this.stream
        },
        async playStream() {
            try {
                await this.$refs.media.play()
                await this.loadSettings()
                this.$emit('update:modelValue', {...this.modelValue, playing: true})
            } catch (message) {
                if (this.glnStream) {
                    this.$m.sfu.delMedia(this.glnStream.id)
                }
                this.app.logger.warn(`stream terminated unexpectedly: ${message}`)

            }
        },
        setFullscreen() {
            this.$refs.media.requestFullscreen()
        },
        setPip() {
            if (this.pip.active) {
                document.exitPictureInPicture()
            } else {
                this.$refs.media.requestPictureInPicture()
            }
        },
        toggleEnlarge() {
            for (const stream of $s.streams) {
                if (stream.id !== this.modelValue.id) {
                    stream.enlarged = false
                }
            }
            this.$emit('update:modelValue', {...this.modelValue, enlarged: !this.modelValue.enlarged})
        },
        toggleMuteVolume() {
            this.muted = !this.muted
            this.$refs.media.muted = this.muted
        },
        toggleStats() {
            this.stats.visible = !this.stats.visible
        },
        toggleStreamBar(active) {
            this.bar.active = active
        },
    },
    mounted() {
        // Directly set the default aspect-ratio; the loadedmetadata event takes
        // a while before the stream knows its eventual video aspect ratio.
        this.$refs.root.style.setProperty('--aspect-ratio', this.modelValue.aspectRatio)
        // Firefox doesn't support this API (yet).
        if (this.$refs.media.requestPictureInPicture) {
            // this.pip.enabled = true

            this.$refs.media.addEventListener('enterpictureinpicture', () => { this.pip.active = true })
            this.$refs.media.addEventListener('leavepictureinpicture', () => { this.pip.active = false })
        }

        this.$refs.media.addEventListener('loadedmetadata', () => {
            // Use the default aspectRatio for audiotracks
            if (this.$refs.media.videoHeight) {
                const aspectRatio = this.$refs.media.videoWidth / this.$refs.media.videoHeight
                this.$refs.root.style.setProperty('--aspect-ratio', aspectRatio)
                this.$emit('update:modelValue', {...this.modelValue, aspectRatio})
            }
        })

        this.muted = this.$refs.media.muted

        if (this.modelValue.direction === 'up') this.mountUpstream()
        else this.mountDownstream()

    },
    props: {
        controls: {
            default() {return true},
            type: Boolean,
        },
        modelValue: {
            required: true,
            type: Object,
        },
    },
    render() {
        return (
            <div                
                class={classes('c-stream', {
                    audio: this.modelValue.hasAudio && !this.modelValue.hasVideo,
                    enlarged: this.modelValue.enlarged,
                    loading: !this.modelValue.playing,
                })}
                onMouseout={this.toggleStreamBar(false)}
                onMouseover={this.toggleStreamBar(true)}
                ref="root" 
            >
                <video
                    ref="media"
                    autoplay={true}
                    class={classes('media', {'media-failed': this.mediaFailed, mirror: this.modelValue.mirror})}
                    muted={this.modelValue.direction === 'up'}
                    playsinline={true}
                    onClickStop={this.toggleEnlarge()}
                />

                {(() => {
                    if (!this.modelValue.playing) {
                        return (
                            <div class="loading-container">
                                <Icon class="spinner" name="Spinner" />
                            </div>
                        )
                    } else if (!this.modelValue.hasVideo) {
                        return (
                            <div class="media-container">
                                <Icon name="Logo" />
                            </div>
                        )
                    }
                })()}

                {this.stats.visible && <Reports description={this.modelValue} onClickStop={this.toggleStats} />}

                {this.controls && this.modelValue.playing && <div class="user-info">
                    {this.audioEnabled && <SoundMeter
                        class="soundmeter"
                        orientation="vertical"
                        stream={this.stream}
                        stream-id={this.stream.id}
                    />}

                    {this.audioEnabled && this.modelValue.direction === 'down' && (
                        <div
                            v-tip={{content: `${this.volume.value}% ${$t('device.audio_volume')}, ${$t('ui.action.double_click', {achievement: this.volume.locked ? $t('group.unlock') : $t('group.lock')})}`}}
                            class="volume-slider"
                        >
                            <FieldSlider v-model={this.volume} />
                        </div>
                    )}

                    <div class={classes('user', {'has-audio': this.audioEnabled})}>
                        {this.modelValue.username}
                    </div>
                </div>}

                <div class={classes('stream-options', {active: this.bar.active})}>
                    {this.pip.enabled && <Button
                        icon="Pip"
                        size="s"
                        tip={$t('stream.pip')}
                        variant="menu"
                        onClickStop={this.setPip}
                    />}

                    <Button
                        icon="Fullscreen"
                        size="s"
                        tip={$t('stream.fullscreen')}
                        variant="menu"
                        onClickStop={this.setFullscreen}
                    /> 

                    {this.hasSettings && <Button
                        active={this.stats.visible}
                        icon="Info"
                        size="s"
                        tip={$t('stream.info')}
                        variant="menu"
                        onClickStop={this.toggleStats}
                    />}
                </div>
            </div>
        )
    },
    watch: {
        'modelValue.volume.value'(value) {
            this.$refs.media.volume = value / 100
        },
    },
})
