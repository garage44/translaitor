import {defineComponent} from '@/lib/component'
import {required} from '@vuelidate/validators'
import useVuelidate from '@vuelidate/core'
import {Button, FieldCheckbox, FieldRadioGroup, FieldSelect, FieldText, Hint, Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export const Login = defineComponent({
    computed: {
        btnLoginDisabled() {
            if (this.busy || this.v$.$error) {
                return true
            }
            // Server validation should not disable the login button.
            const hasErrors = this.v$.$silentErrors.filter((v) => v.$validator !== '$externalResults').length > 0
            return hasErrors
        },
        currentGroup() {
            return this.$m.group.currentGroup()
        },
        isListedGroup() {
            return !!$s.groups.find((i) => i.name === $s.group.name)
        },
    },

    data() {
        return {
            authOptions: [
                ['user', $t('user.name')],
                ['guest', $t('user.guest')],
                ['anonymous', $t('user.anonymous')],
            ],
            busy: false,
            devices: $s.devices,
            guestToggle: ['user', 'guest'],
            user: $s.user,
            vuelidateExternalResults: {
                user: {
                    password: [],
                    username: [],
                },
            },
        }
    },
    methods: {
        clearCredentials() {
            $s.user.username = ''
            $s.user.password = ''
            this.app.store.save()
        },
        async login() {
            this.vuelidateExternalResults.user.username = []
            this.vuelidateExternalResults.user.password = []

            this.busy = true
            try {
                if ($s.user.authOption === 'user') {
                    await this.$m.sfu.connect($s.user.username, $s.user.password)
                } else if ($s.user.authOption === 'guest') {
                    await this.$m.sfu.connect($s.user.username, '')
                } else if ($s.user.authOption === 'anonymous') {
                    await this.$m.sfu.connect('', '')
                }
            } catch (err) {
                if (err === 'group is locked') {
                    this.app.notifier.notify({
                        level: 'error',
                        message: $t('fail_locked', {group: $s.group.name}),
                    })
                    this.vuelidateExternalResults.user.username = [$t('user.login.validate_locked')]
                    this.vuelidateExternalResults.user.password = [$t('user.login.validate_locked')]
                } else if (err === 'not authorised') {
                    const message = $t('user.login.fail_credentials', {group: $s.group.name})
                    this.app.notifier.notify({level: 'error', message})
                    this.vuelidateExternalResults.user.username = [message]
                    this.vuelidateExternalResults.user.password = [message]
                    this.v$.$validate()
                }
            }

            this.busy = false
            if (this.v$.$invalid) return

            // Save credentials for the next time.
            this.app.store.save()

            $s.group.connected = true
            this.$router.replace({
                name: 'conference-groups-connected',
                params: {groupId: this.$router.currentRoute.value.params.groupId},
            })
        },
        setAuthOption(currentGroup) {
            if (currentGroup.locked) {
                $s.user.authOption = 'user'
            } else {
                // Username already filled? Assume user authentication.
                if ($s.user.username) {
                    $s.user.authOption = 'user'
                } else if (currentGroup['allow-anonymous'] && currentGroup['public-access']) {
                    $s.user.authOption = 'anonymous'
                } else if (currentGroup['public-access'] && !currentGroup['allow-anonymous']) {
                    $s.user.authOption = 'guest'
                } else {
                    $s.user.authOption = 'user'
                }
            }
        },
    },
    async mounted() {
        const currentGroup = this.$m.group.currentGroup()
        this.setAuthOption(currentGroup)

        this.busy = true
        await this.$m.media.queryDevices()
        this.busy = false
    },
    render() {
        if (!this.currentGroup) return
        return (
            <div class="c-login content" onKeypressEnter={this.login}>
                <header>
                    <div class="notice">
                        {this.currentGroup.locked && <Hint class="field" text={$t('user.login.locked_maintenance')} />}

                    </div>
                    <div class="title">
                        <span>{this.$route.params.groupId}</span>
                        <Icon class="icon icon-regular" name={this.currentGroup.locked ? 'GroupLocked' : 'Group'} />
                    </div>
                </header>
                <div class="panels">
                    <section>
                        <form autocomplete="off">
                            {(() => {
                                if (!this.currentGroup.locked && (this.currentGroup['public-access'] && !this.currentGroup['allow-anonymous'])) {
                                    return <FieldCheckbox
                                        v-model={$s.user.authOption}
                                        label={$t('user.login.as_guest')} toggle={this.guestToggle}
                                    />
                                } else if (!this.currentGroup.locked && (this.currentGroup['allow-anonymous'] && this.currentGroup['public-access'])) {
                                    return <FieldRadioGroup
                                        v-model={$s.user.authOption}
                                        label={$t('user.login.as')} options={this.authOptions}
                                    />
                                }
                            })()}

                            {!this.isListedGroup && <FieldText
                                v-model={this.currentGroup.name}
                                label={$t('group.name')}
                                name="groupname"
                            />}

                            {['user', 'guest'].includes($s.user.authOption) && <FieldText
                                v-model={this.v$.user.username.$model}
                                autocomplete="off"
                                autofocus={$s.login.autofocus && this.$route.params.groupId}
                                label={$t('user.username')}
                                name="username"
                                placeholder={$t('user.username_placeholder')}
                                validation={this.v$.user.username}
                            />}

                            {$s.user.authOption === 'user' && <FieldText
                                v-model={this.v$.user.password.$model}
                                autocomplete="password"
                                label={$t('user.password')}
                                name="pasword"
                                placeholder={$t('user.password_placeholder')}
                                type="password"
                                validation={this.v$.user.password}
                            />}

                            {this.currentGroup.contact && <div class="group-contact">
                                <Icon v-tip={{content: $t('group.contact')}} class="icon-d" name="Administrator" />
                                {this.currentGroup.contact}
                            </div>}

                            {this.currentGroup.comment && <div class="group-comment field">
                                <div class="field-label">
                                    {$t('group.about')} {this.currentGroup.name}
                                </div>
                                <div class="comment">
                                    {this.currentGroup.comment}
                                </div>
                            </div>}

                            <FieldSelect
                                v-model={this.v$.devices.cam.selected.$model}
                                disabled={!$s.devices.cam.enabled}
                                help={$t('device.select_cam_help')}
                                label={$t('device.select_cam_label')}
                                name="cam"
                                options={$s.devices.cam.options}
                                validation={this.v$.devices.cam.selected.id}
                            >
                                {/* <template #header>
                                    <FieldCheckbox v-model="$s.devices.cam.enabled" />
                                </template> */}
                            </FieldSelect>

                            <FieldSelect
                                v-model={this.v$.devices.mic.selected.$model}
                                disabled={!$s.devices.mic.enabled}
                                help={$t('device.select_mic_help')}
                                label={$t('device.select_mic_label')}
                                name="audio"
                                options={$s.devices.mic.options}
                                validation={this.v$.devices.mic.selected.id}
                            >
                                {/* <template #header>
                                    <FieldCheckbox v-model={$s.devices.mic.enabled} />
                                </template> */}
                            </FieldSelect>

                            {!$s.env.isFirefox && <div class="media-option">
                                <FieldSelect
                                    v-model={this.v$.devices.audio.selected.$model}
                                    help={$s.env.isFirefox ? $t('ui.unsupported_option', {browser: $s.env.browserName}) : $t('device.select_audio_help')}
                                    label={$t('device.select_audio_label')}
                                    name="audio"
                                    options={$s.devices.audio.options}
                                    validation={this.v$.devices.audio.selected.id}
                                />
                            </div>}
                        </form>

                        <div class="verify">
                            <RouterLink
                                to={{name: 'conference-settings', params: {tabId: 'devices'}}}>
                                {$t('group.verify_devices')}
                            </RouterLink>
                        </div>
                    </section>
                    <div class="actions">
                        <Button
                            disabled={this.btnLoginDisabled}
                            icon="Login"
                            tip={this.currentGroup.locked ? $t('group.join_locked') : $t('group.join')}
                            variant="menu"
                            onClick={this.login}
                        />

                        <Button
                            disabled={!$s.user.username && !$s.user.password}
                            icon="Backspace"
                            tip={$t('user.action.clear_credentials')}
                            variant="menu"
                            onClick={this.clearCredentials}
                        />
                    </div>
                </div>
            </div>
        )
    },
    setup() {
        return {v$: useVuelidate()}
    },
    validations() {
        const validations = {}
        if ($s.user.authOption === 'user') {
            validations.user = {
                password: {required},
                username: {required},
            }

        } else if ($s.user.authOption === 'guest') {
            validations.user = {
                username: {required},
            }
        }

        validations.devices = {
            audio: {selected: {id: {}}},
            cam: {selected: {id: {}}},
            mic: {selected: {id: {}}},
        }

        if ($s.devices.cam.enabled) {
            validations.devices.cam.selected.id = {required}
        }

        if ($s.devices.mic.enabled) {
            validations.devices.mic.selected.id = {required}
        }

        if ($s.devices.audio.enabled && !$s.env.isFirefox) {
            validations.devices.audio.selected.id = {required}
        }

        return validations

    },
    watch: {
        '$s.devices': {
            handler() {
                this.app.store.save()
            },
            deep: true,
        },
        '$s.user': {
            handler() {
                this.vuelidateExternalResults.user.username = []
                this.vuelidateExternalResults.user.password = []
            },
            deep: true,
        },
        currentGroup: {
            handler(currentGroup) {
                this.vuelidateExternalResults.user.username = []
                this.vuelidateExternalResults.user.password = []
                this.setAuthOption(currentGroup)
            },
            deep: true,
        },
    },
})
