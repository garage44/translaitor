import {Icon} from '@/components/elements'
import {Stream} from '../stream/stream'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'
import {$s} from '@/lib/main'

export const Group = defineComponent({
    beforeUnmount() {
        this.resizeObserver.disconnect()
    },
    beforeUpdate() {
        this.itemRefs = []
    },
    computed: {
        sortedStreams() {
            const streams = $s.streams
            return streams.sort((a, b) => {
                if (a.username < b.username){
                    return -1
                }
                if (a.username > b.username) {
                    return 1
                }
                return 0
            })
        },
        streamsCount() {
            // All streams; loading & playing
            return $s.streams.length
        },
        streamsPlayingCount() {
            // Recalculate the stream canvas; the aspect-ratio of one
            // the streams may have been changed (see stream loadedmetadata)
            return $s.streams.filter((s) => s.playing).length
        },
    },
    components: {Stream},
    data() {
        return {
            // This is not the stream's aspect ratio; but the aspect-ratio / stream
            // container to calculate with. I'm not sure if it may be feasible to
            // calculate with aspect-ratio per stream here.
            aspectRatio: 4 / 3,
            margin: 16,
        }
    },
    async mounted() {
        this.$refs.view.style.setProperty('--stream-margin', `${this.margin}px`)
        this._calcLayout = this.calcLayout.bind(this)
        this.resizeObserver = new ResizeObserver(async() => {
            requestAnimationFrame(this._calcLayout)
        })

        requestAnimationFrame(this._calcLayout)
        this.resizeObserver.observe(this.$refs.view)
        // At this point, the chat history is already loaded and
        // we set the active channel. This circumvents unwanted
        // unread messages (see chat model's onMessage).
        $s.chat.channel = 'main'
    },
    methods: {
        /**
         * Optimal space algorithm from Anton Dosov:
         * https://dev.to/antondosov/building-a-video-gallery-just-like-in-zoom-4mam
         * Also, learned from this resource:
         * https://github.com/Alicunde/Videoconference-Dish-CSS-JS
         */
        calcLayout() {
            if (!this.$refs.view) return
            const containerWidth = this.$refs.view.offsetWidth
            const containerHeight = this.$refs.view.offsetHeight
            let layout = {area: 0, cols: 0, height: null, rows: 0, width: null}
            let height, width

            for (let cols = 1; cols <= $s.streams.length; cols++) {
                const rows = Math.ceil($s.streams.length / cols)
                const hScale = containerWidth / (cols * this.aspectRatio)
                const vScale = containerHeight / rows

                // Determine which axis is the constraint.
                if (hScale <= vScale) {
                    width = Math.floor((containerWidth - this.margin) / cols) - this.margin
                    height = Math.floor(width / this.aspectRatio) - this.margin
                } else {
                    height = Math.floor((containerHeight - this.margin) / rows) - this.margin
                    width = Math.floor(height * this.aspectRatio) - this.margin
                }

                const area = width * height
                if (area > layout.area) {
                    layout = {area, cols, height, rows, width}
                }
            }

            this.$refs.view.style.setProperty('--stream-width', `${layout.width}px`)
        },
    },
    render() {
        return (
            <div ref="view" class="c-group">
                {/* <router-view v-slot={Component}>
                    <Transition>
                        <component is={Component} />
                    </Transition>
                </router-view> */}

                {this.sortedStreams.map((description, index) => {
                    return <Stream modelValue={this.sortedStreams[index]}/>
                })}
        
                <Icon v-if="!$s.streams.length" class="icon logo-animated" name="Logo" />
            </div>
        )
    },
    watch: {
        async streamsCount() {
            requestAnimationFrame(this.calcLayout.bind(this))
        },
        async streamsPlayingCount() {
            requestAnimationFrame(this.calcLayout.bind(this))
        },
    },
})
