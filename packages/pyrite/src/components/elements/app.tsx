import {defineComponent} from '@/lib/component'
import {RouterView} from 'vue-router'
import {h} from 'vue'

export default defineComponent({
    mounted() {
        let displayMode = 'browser'
        const mqStandAlone = '(display-mode: standalone)'
        if (navigator.standalone || window.matchMedia(mqStandAlone).matches) {
            displayMode = 'standalone'
        }

        // The title in standalone modus is set by the manifest name.
        if (displayMode === 'browser') {
            document.title = 'Pyrite Video'
        }
    },
    render() {
        return <RouterView />
    },
})
