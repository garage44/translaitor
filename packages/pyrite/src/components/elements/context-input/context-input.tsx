import {defineComponent} from '@/lib/component'
import {FieldText, Icon} from '@/components/elements'
import {h} from 'vue'

export const ContextInput = defineComponent({
    computed: {
        _icon() {
            if (typeof this.icon === 'string') return this.icon
            return this.icon()
        },
        _title() {
            if (!this.title) return null
            if (typeof this.title === 'string') return this.title
            return this.title()
        },
    },
    data() {
        return {
            input: false,
            inputTransition: false,
            text: '',
        }
    },
    emits: ['submit'],
    methods: {
        buttonAction() {
            if (this.revert) {
                this.submitMethod()
            } else {
                this.input = !this.input
            }
        },
        inputTransitioned() {
            this.inputTransition = this.input
        },
        async submitMethod() {
            await this.submit(this.text)
            // Reset text and hide input again.
            this.input = false
            this.text = ''
        },
    },
    props: {
        icon: {
            default: () => 'Kick',
            required: false,
            type: [Function, String],
        },
        required: {
            default: () => true,
            required: false,
            type: Boolean,
        },

        /**
         * Revert is a way to toggle the previous
         * state of an action, without having to go
         * through the input state.
         */
        revert: {
            default: () => false,
            required: false,
            type: Boolean,
        },
        submit: {
            default: () => {},
            required: false,
            type: Function,
        },
        title: {
            default: () => 'No title',
            required: false,
            type: [Function, String],
        },
    },
    render() {
        return (
            <div class="c-context-input">
                <transition mode="out-in" name="c-context-input-fade" onAfter-leave="inputTransitioned">
                    {(() => {
                        if (this.input && !this.revert) {
                            return (
                                <div class="action-input">
                                    <FieldText
                                        v-model="text"
                                        autofocus={this.inputTransition}
                                        onKeyupEnter={this.submitMethod}
                                    />

                                    {(() => {
                                        if (this.required && this.text === '') {
                                            return (
                                                <button class="btn" onClick={() => this.input = !this.input}>
                                                    <Icon
                                                        v-tip="{content: $t('ui.close')}"
                                                        class="icon icon-s"
                                                        name="Close"
                                                    />
                                                </button>
                                            )
                                        } else {
                                            return (
                                                <button class="btn" onClick="submitMethod">
                                                    <Icon
                                                        v-tip="{content: $t('ui.submit')}"
                                                        class="icon icon-s"
                                                        name="Send"
                                                    />
                                                </button>
                                            )
                                        }
                                    })()}
                                </div>
                            )
                        } else if (this._title) {
                            return (
                                <button class="action" onClick="buttonAction">
                                    <Icon
                                        class="icon icon-s"
                                        name={this._icon}
                                    />
                                    {this._title && <span>{this._title}</span>}
                                    
                                </button>
                            )
                        }
                    })()}
                </transition>
            </div>
        )
    },
})

