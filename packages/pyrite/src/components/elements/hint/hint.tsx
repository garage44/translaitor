import {Icon} from '@/components/elements'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const Hint = defineComponent({
    props: {
        text: {
            type: String,
            default() {return ''},
        },
    },
    render() {
        return (
            <div class="c-hint">
                <Icon class="item-icon icon-d" name="Info" />
                <div class="description">
                    {this.text}
                </div>
            </div>
        )
    },
})

