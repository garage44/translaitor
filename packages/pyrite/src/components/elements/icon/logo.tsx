import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const Logo = defineComponent({
    data: function() {
        return {
            custom: ['Chat', 'Logo'],
            svg,
        }
    },
    props: {
        iconProps: {
            default() {return false},
            required: false,
            type: [Object, Boolean],
        },
        name: {
            required: true,
            type: String,
        },
    },
    render() {
        return [
            <path
                id="path1883"
                class="shard dark"
                d="m 5.4804785,14.410944 a 7.1183372,7.1183372 0 0 0 5.2827995,4.444952 l 2.565838,-4.444952 z"
            />,
            <path
                id="path1881"
                class="shard primary"
                d="m 15.191999,12.207584 -3.886627,6.73271 a 7.1183372,7.1183372 0 0 0 0.824501,0.04797 7.1183372,7.1183372 0 0 0 5.486219,-2.58279 z"
            />,
            <path
                id="path1879"
                class="shard dark"
                d="M 14.215653,9.4927872 17.952241,15.96509 A 7.1183372,7.1183372 0 0 0 19.248144,11.869992 7.1183372,7.1183372 0 0 0 18.839139,9.4927872 Z"
            />,
            <path
                id="path1877"
                class="shard primary"
                d="m 6.6768355,7.2944764 a 7.1183372,7.1183372 0 0 0 -1.665233,4.5755156 7.1183372,7.1183372 0 0 0 0.295753,2.029876 h 5.1825315 z"
            />,
            <path
                id="path1875"
                class="shard primary"
                d="m 13.714316,4.9306159 -2.338973,4.0510954 h 7.260376 A 7.1183372,7.1183372 0 0 0 13.714316,4.9306159 Z"
            />,
            <path
                id="path1841"
                class="shard dark"
                d="M 12.129873,4.7517214 A 7.1183372,7.1183372 0 0 0 7.0378715,6.8962918 L 9.5131795,11.18435 13.181961,4.8299879 a 7.1183372,7.1183372 0 0 0 -1.052088,-0.078266 z"
            />,
            <circle
                id="circle1843"
                class="pupil primary"
                cx="12.129838"
                cy="11.870106"
                r="1.5759634"
            />,
        ]
    },
})
