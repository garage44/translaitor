import {classes} from '@/lib/utils'
import {$s} from '@/lib/main'
import {defineComponent} from '@/lib/component'
import {Icon, Progress} from '@/components/elements'
import {h} from 'vue'
import {$t} from '@/lib/main'

export const Notifications = defineComponent({
    methods: {
        buttonClick(notification, button) {
            button.action(notification, button)
        },
        closeNotification(notification) {
            $s.notifications.splice($s.notifications.findIndex(i => i.id === notification.id), 1)
        },
        openUrl(url) {
            window.open(url, '_blank')
        },
    },
    render() {   
        return <div class="c-notifications">
            {$s.notifications.map((notification) => {
                return (
                    <div class={classes('notification', {[notification.level]: true})}>
                        <div class="message">
                            <Icon 
                                class="icon icon-d" 
                                name={notification.icon ? notification.icon : notification.level[0].toUpperCase() + notification.level.slice(1)}
                            />
                            <div class="text-wrapper">
                                {notification.progress && <Progress 
                                    boundaries={notification.progress.boundaries} 
                                    percentage={notification.progress.percentage} 
                                />}
                            </div>                                    
        
                            <div class="message-text">
                                <span>{notification.message}</span>
                                {notification.link && (
                                    <span class="cf link" onClick={this.openUrl(notification.link.url)}>
                                        {notification.link.text}
                                    </span>
                                )}
                                {notification.list && <ul class="message-list">
                                    {notification.list.map((item) => <li>{item}</li>)}
                                </ul>}

                                {notification.personal && <div class="personal">
                                    <div class="op">
                                        {(() => {
                                            if (notification.personal.op) {
                                                return <span>
                                                    {$t('user.operator')} {notification.personal.op}:
                                                </span>
                                            } else if (notification.personal.group) {
                                                return <span>{$t('group.name')} {notification.personal.group}:</span>
                                            }
                                        })()}
                                    </div>
                                    <div class="message-text">
                                        {notification.personal.message}
                                    </div>
                                </div>}
                            </div>

                            <div class="buttons">
                                {notification.buttons && notification.buttons.map((button) => {
                                    return (
                                        <button class="btn" onClick={this.buttonClick(notification, button)}>
                                            <Icon class="icon-d" name={button.icon} />
                                            <span>{button.text}</span>
                                        </button>
                                    )
                                })}
                            </div>
                        </div>
        
                        <Icon 
                            class="icon icon-d btn-close" 
                            name="Close" 
                            onClick={() => this.closeNotification(notification)} 
                        />
                    </div>
                )
            })}
        </div>
    }
})
