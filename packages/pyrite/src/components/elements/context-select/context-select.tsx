import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const ContextSelect = defineComponent({
    computed: {
        description() {
            let title = this.title
            return title
        },
        model: {
            get() {return this.modelValue},
            set(modelValue) {this.$emit('update:modelValue', modelValue)},
        },
    },
    data() {
        return {
            input: false,
            text: '',
        }
    },
    emits: ['update:modelValue'],
    methods: {
        async buttonAction() {
            this.input = !this.input
            this.$refs.select.toggleSelect(null, null, true)
        },
        inputTransitioned() {
            this.inputTransition = this.input
        },
        async submitMethod() {
            await this.submit(this.model)
            this.input = false
        },
    },
    props: {
        icon: {
            required: true,
            type: String,
        },
        modelValue: {
            required: true,
            type: Object,
        },
        options: {
            required: true,
            type: Array,
        },
        submit: {
            required: true,
            type: Function,
        },
        title: {
            required: true,
            type: String,
        },
    },
    render() {
        return (
            <div class="c-context-select">
                <button class="action" onClickStop="buttonAction">
                    <Icon class="icon icon-s" name={this.icon} /><span>{this.title}</span>
                </button>
        
                <FieldSelect
                    v-show="input"
                    ref="select"
                    v-model="model"
                    options={this.options}
                />
            </div>
        )
    },
    watch: {
        model() {
            if (this.input) this.submitMethod()
        },
    },
})
