import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export const Splash = defineComponent({
    computed: {
        subtitle() {
            if (this.instruction) {
                if (typeof this.instruction === 'function') return this.instruction()
                else return this.instruction
            }
            else return $t('ui.conferencing')
        },
        title() {
            if (this.header) {
                return this.header
            } else {
                return 'pyrite'
            }
        },
    },
    mounted() {
        if (!$s.group.connected) {
            $s.group.name = ''
        }
    },
    props: {
        header: {
            required: false,
            default: () => '',
            type: String,
        },
        instruction: {
            required: false,
            default: () => '',
            type: [Function, String],
        },
    },
    render() {
        return (
            <div class="c-splash">
                <Icon class="icon logo-animated" name="Logo" />
                <div class="title uca">
                    {this.title}
                </div>
                <div class="subtitle uca">
                    {this.subtitle}
                </div>
            </div>
        )
    },
})

