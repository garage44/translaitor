import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {FieldTextarea, Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'

export const Button = defineComponent({
    computed: {
        classNames() {
            const classes = {'c-button': true}
            if (this.active === true || this.contextTriggered) {
                classes.active = true
            }

            classes[`intensity-${this.intensity}`] = true
            classes[`size-${this.size}`] = true
            classes[`variant-${this.variant}`] = true
            classes[`type-${this.type}`] = true

            return classes
        },
        iconSize() {
            if (this.variant === 'toggle') {
                return 'icon-s'
            }

            return 'icon-d'
        },
    },
    data() {
        return {
            contextModel: '',
            contextTriggered: false,
        }
    },
    methods: {
        disableContext() {
            this.contextTriggered = false
        },
        handleClick() {
            if (!this.context) return
            if (!this.contextTriggered) {
                if (this.context.enabled) {
                    this.contextTriggered = true
                } else {
                    // No context action; just submit the regular action.
                    this.context.submit(this.contextModel)
                }
            }
        },
        handleSubmit() {
            this.contextTriggered = false
            this.context.submit(this.contextModel)
            this.contextModel = ''
        },
    },
    props: {
        active: {
            default: () => false,
            type: [Boolean, null],
            required: false,
        },
        context: {
            default: null,
            type: [Object, null],
            required: false,
        },
        icon: {
            default: () => '',
            required: false,
            type: String || Object,
        },
        iconProps: {
            default: () => {},
            required: false,
            type: Object,
        },
        intensity: {
            default: () => 'medium',
            required: false,
            type: String,
        },
        route: {
            default: () => {},
            required: false,
            type: Object,
        },
        size: {
            default: () => 'd',
            required: false,
            type: String,
        },
        tip: {
            default: () => '',
            required: false,
            type: String,
        },
        type: {
            default: () => 'info',
            required: false,
            type: String,
        },
        variant: {
            default: () => 'default',
            required: false,
            type: String,
        },
    },
    setup() {
        return function() {
            if (this.route) {                
                return (
                    <RouterLink
                        class={this.classNames}
                        to={this.route}
                    >
                        {() => {
                            if (this.icon) {
                                return <Icon
                                    class={this.iconSize}
                                    icon-props={this.iconProps}
                                    name={this.icon}
                                />
                            }
                        }}
                    </RouterLink>
                )
            }

            return (
                <div
                    v-click-outside={this.disableContext}
                    v-tip={{content: this.tip}}
                    class={this.classNames}
                    onClick={this.handleClick}
                >

                    {this.icon && <Icon
                        class={this.iconSize}
                        icon-props={this.iconProps}
                        name={this.icon}
                    />}

                    {this.context && 
                        <div class={classes({
                            active: this.contextTriggered, 
                            'context-submit': true})
                        }>
                            <button class="btn-context-submit" onClick="handleSubmit">
                                <Icon class="icon icon-s" name="Check" />
                            </button>
                
                            <FieldTextarea
                                v-model="contextModel"
                                placeholder={this.context.placeholder}
                                onClick=""
                            />
                        </div> 
                    }
                </div>
            )
        }
    },
})

