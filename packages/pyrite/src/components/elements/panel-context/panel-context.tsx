import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {Icon} from '@/components/elements'
import {RouterLink} from 'vue-router'
import {h} from 'vue'
import {$s, $t} from '@/lib/main'

export const PanelContext = defineComponent({
    computed: {
        toggleContext() {
            if (this.$route.name === 'conference-splash') {
                return {name: 'admin-groups-settings'}
            } else if (this.$route.name === 'conference-groups-connected') {
                return {name: 'admin-groups-settings'}
            } else if (this.$route.name === 'conference-groups-disconnected') {
                // Use the selected group with the group in admin.
                const groupId = this.$route.params.groupId
                return {name: 'admin-groups-settings', params: {groupId, tabId: 'misc'}}
            } else if (this.$route.name.startsWith('admin-group')) {
                if ($s.group.connected) {
                    const groupId = $s.group.name
                    return {name: 'conference-groups-connected', params: {groupId}}
                } else {
                    const groupId = this.$route.params.groupId

                    if (!groupId) {
                        return {name: 'conference-splash'}
                    } else {
                        return {name: 'conference-groups-disconnected', params: {groupId}}
                    }
                }

            }
            return {name: 'conference-main'}
        },
    },
    data() {
        return {
            version: process.env.PYR_VERSION,
        }
    },
    methods: {
        closeContext(el, done) {
            el.style.width = `${$s.chat.width}px`
            this.chatToggle = true
            this.app.animate({
                duration: 350,
                from: $s.chat.width,
                onFinish: () => {
                    this.chatToggle = false
                    done()
                },
                onUpdate: v => {
                    el.style.width = `${Math.floor(v)}px`
                },
                to: 0,
            })
        },
    },
    setup(_, {slots}) {        
        return function() {
            return (
                <div ref="panel" class={classes('c-panel-context', {collapsed: false})}>
                    <header>
                        {(() => {
                            if (!$s.group.connected) {
                                return <RouterLink
                                    // v-tip={{content: this.$route.name.includes('conference-') ? $t('ui.switch_admin') : $t('ui.switch_conference')}}
                                    class="logo"
                                    to={this.toggleContext}
                                >
                                    {() => {
                                        return [                                    
                                            <Icon class="icon" name="Logo" />,
                                            <div class="l-name">
                                                <div class="name">
                                                PYRITE
                                                </div>
                                                <div class="version">
                                                    {this.version}
                                                </div>
                                            </div>,
                                        ]
                                    }}
                                </RouterLink>
                            } else {
                                return <span class="logo">
                                    <Icon class="icon" name="Logo" />
                                    <div class="l-name">
                                        <div class="name">
                                        PYRITE
                                        </div>
                                        <div class="version">
                                            {this.version}
                                        </div>
                                    </div>
                                </span>
                            }
                        })()}
                    </header>
                    {slots.default()}
                </div>
            )
        }
    },
    watch: {
        '$s.env.layout': function(layout) {
            if (layout === 'tablet') {
                $s.panels.context.collapsed = true
            }
        },
        '$s.panels.context.collapsed': function() {
            const spacer = Number(getComputedStyle(document.querySelector('.app')).getPropertyValue('--spacer').replace('px', ''))
            this.app.animate({
                duration: 350,
                from: $s.panels.context.collapsed ? 300 : spacer * 8,
                onFinish: () => {

                },
                onUpdate: v => {
                    this.$refs.panel.style.width = `${Math.floor(v)}px`
                },
                to: $s.panels.context.collapsed ? spacer * 8 : 300,
            })
        },
    },
})

