import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export  const FieldMultiSelect = defineComponent({
    beforeMount() {
        this.uniqueId = Math.random().toString(36).substr(2, 9)
    },
    computed: {
        model: {
            get: function() {
                return this.modelValue
            },
            set: function(value) {
                this.$emit('update:modelValue', value)
            },
        },
    },
    emits: ['update:modelValue'],
    props: {
        help: {
            default: () => '',
            type: String,
        },
        label: {
            default: () => 'Label me',
            type: String,
        },
        modelValue: {
            required: true,
            type:Object,
        },
        options: {
            default: () => [],
            type: Array,
        },
    },
    render() {
        return (
            <div class="c-field-multiselect field">
                <div class="label-container">
                    <label class="field-label" for={this.uniqueId}>{this.label}</label>
                    <slot class="label-extra" name="header" />
                </div>
                <select id={this.uniqueId} v-model="model" multiple>
                    {this.options.map((option) => {
                        return (<option value={option.id}>{option.name}</option>)
                    })}
                </select>
                {this.help && <div class="help">{this.help}</div>}
            </div>
        )
    },
})
