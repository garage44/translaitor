import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import Field from './field'
import {h} from 'vue'
import {$t} from '@/lib/main'

export const FieldSelect = defineComponent({
    beforeMount() {
        this.uniqueId = Math.random().toString(36).substr(2, 9)
    },
    computed: {
        className() {
            const classes = {}
            if (this.validation) {
                classes.validation = true
                if (this.validation.$invalid && this.validation.$dirty) {
                    classes.invalid = true
                }
            }
            return classes
        },
        currentOption() {
            if (this.value && this.value.id) {
                const currentOption = this.options.find((o) => o.id === this.value.id)

                if (currentOption) {
                    return currentOption.name
                }
            }
            return this.placeholder
        },
    },
    data: function() {
        return {
            active: false,
            searchQuery: '',
            searchSelected: this.value,
            selectedOption: null,
        }
    },
    emits: ['update:modelValue'],
    extends: Field,
    methods: {
        emptySelectOption: function() {
            // Handle syncing an empty option to the model.
            let emptyOption = {id: null, name: null}
            // Use the first option to determine additional keys.
            if (this.options.length) {
                for (let key of Object.keys(this.options[0])) {
                    emptyOption[key] = null
                }
            }
            return emptyOption
        },
        handleEvent(option, keyModifier, updateModel) {
            this.active = true

            if (option) {
                // Option click select.
                this.selectedOption = option
            } else if (keyModifier) {
                this.navigate(keyModifier)
            } else {
                // Click/focus.
                if (!this.searchSelected.id) this.selectedOption = this.options[0]
                else this.selectedOption = this.searchSelected
            }

            if (this.selectedOption) {
                this.searchSelected = this.selectedOption
                if (updateModel) {
                    if (this.validation) this.validation.$touch()
                    this.searchQuery = ''
                    this.active = false
                    this.searchPlaceholder = this.selectedOption.name
                    this.$emit('update:modelValue', {...this.selectedOption})
                } else {
                    this.active = true
                }
            }
        },
        navigate(keyModifier) {
            if (keyModifier === 'enter') {
                if (!this.searchSelected.id) this.selectedOption = this.options[0]
                else {
                    this.selectedOption = this.searchSelected
                }
            } else if (['up', 'down', 'page-down', 'page-up'].includes(keyModifier)) {
                if (!this.searchSelected.id) this.selectedOption = this.options[0]
                else {
                    const itemIndex = this.options.findIndex((i) => i.id === this.searchSelected.id)
                    if (keyModifier === 'down' && this.options.length > itemIndex) {
                        this.selectedOption = this.options[itemIndex + 1]
                    } else if (keyModifier === 'up' && itemIndex > 0) {
                        this.selectedOption = this.options[itemIndex - 1]
                    } else if (keyModifier === 'page-down') {
                        if (this.options.length >= itemIndex + 5) {
                            this.selectedOption = this.options[itemIndex + 5]
                        }
                    } else if (keyModifier === 'page-up') {
                        if (this.options.length >= itemIndex - 5 && (itemIndex - 5) >= 0) {
                            this.selectedOption = this.options[itemIndex - 5]
                        }
                    }
                }
            } else if (keyModifier === 'query') {
                this.selectedOption = this.options[0]
            }
        },
        toggleSelect(e, vClickOutside, active) {
            if (typeof vClickOutside === 'object' && !active) {
                this.active = false
                return
            }

            if (active !== undefined) this.active = active
            else this.active = !this.active
        },
        updateModel: function(event) {
            let value = event.target.value
            if (!value) {
                this.$emit('update:modelValue', this.emptySelectOption())
            } else {
                for (const option of this.options) {
                    if (option.id === value) {
                        this.$emit('update:modelValue', {...option})
                    }
                }
            }
        },
    },
    mounted() {
        if (this.validation) this.validation.$touch()
    },
    props: {
        disabled: {
            default: () => false,
            type: Boolean,
        },
        empty: {
            default: 'no options available',
            type: String,
        },
        idfield: {
            default: () => 'id',
            type: String,
        },
        options: {
            default: () => [],
            type: Array,
        },
        placeholder: {
            default: () => '...',
            type: String,
        },
        search: {
            default: false,
            type: Boolean,
        },
        validation: {
            default: () => null,
            required: false,
            type: Object,
        },
        value: {
            required: true,
            type:Object,
        },
    },
    render() {
        return (
            <div 
                v-click-outside={this.toggleSelect.bind(this)} 
                class={classes('c-field-select field', {[this.className]: true})}
            >
                {this.label && (
                    <div class="label-container">
                        <slot class="label-extra" name="header" />
                        <label class="field-label" for={`${this.uniqueId}-${this.name}`}>{$t(this.label)}</label>
                    </div>
                )}
        
                <div class="input-container">
                    <div class="button-wrapper">

                        {(this.options && this.options.length) && (
                            <input
                                id={`${this.uniqueId}-${this.name}`}
                                ref="input"
                                model={this.searchQuery}
                                autocomplete="off"
                                disabled={this.disabled}
                                placeholder={$t(this.currentOption)}
                                readonly={!this.search}
                                onClick={this.handleEvent.bind(this, null, null, false)}
                                onInput={this.handleEvent.bind(this, null, 'query', false)}
                                onKeydownDown={this.handleEvent.bind(this, null, 'down', false)}
                                onKeydownPageDown={this.handleEvent.bind(this, null, 'page-down', false)}
                                onkeydownPageUp={this.handleEvent.bind(this, null, 'page-up', false)}
                                onKeydownUp={this.handleEvent.bind(this, null, 'up', false)}
                                onKeyupEnter={this.handleEvent.bind(this, null, 'enter', true)}
                                onKeyupEscape={() => this.active = false}
                            />  
                        )}
     
                        <slot class="button" name="button" />
                    </div>
        
                    <div
                        ref="options"
                        class={classes('options', {active: this.active})}
                    >
                        {this.options.map((option) => {
                            return (
                                <div
                                    id={`${this.uniqueId}-option-${option.id}`}
                                    class={classes('option', {selected: this.searchSelected.id === option.id})}
                                    onClick={this.handleEvent.bind(this, option, null, true)}
                                >
                                    {$t(option.name)}
                                </div>
                            )
                        })}
                    </div>
                </div>
                {this.validation && (
                    <div class="validation-message">
                        {this.validation.$invalid && this.validation.$dirty && this.validation.$silentErrors.map((error) => {
                            return (<div class="error">{error.$message}</div>)
                        })}
                    </div>
                )}

                {(() => {
                    if (this.searchSelected.help) {
                        return (
                            <div class="help">
                                {$t(this.searchSelected.help)}
                            </div>
                        )
                    } else if (this.help) {
                        return <div class="help">{$t(this.help)}</div>
                    }
                })()}
            </div>
        )
    },
    updated() {
        const input = this.$refs.input
        const options = this.$refs.options

        let selected
        
        if (this.searchSelected.id) {
            for (const childNode of options.childNodes) {
                if (`#${this.uniqueId}-option-${this.searchSelected.id}` === childNode.id) {
                    selected = childNode
                }
            }
        }

        if (selected) {
            options.scrollTop = selected.offsetTop - input.offsetHeight - selected.offsetHeight
        }
    },
})
