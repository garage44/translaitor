import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const FieldNumber = defineComponent({
    beforeMount() {
        this.uniqueId = Math.random().toString(36).substr(2, 9)
    },
    computed: {
        model: {
            get: function() {
                return this.modelValue
            },
            set: function(value) {
                this.$emit('update:modelValue', value)
            },
        },
    },
    emits: ['update:modelValue'],
    props: {
        help: {
            default: () => '',
            type: String,
        },
        label: {
            default: () => 'Label me',
            type: String,
        },
        modelValue: {
            required: true,
            type:Number,
        },
        options: {
            default: () => [],
            type: Array,
        },
        placeholder: {
            default: () => '...',
            type: String,
        },
    },
    render() {
        return (
            <div class="c-field-number field">
                <div class="label-container">
                    <label class="field-label" for={this.uniqueId}>{this.label}</label>
                    <slot class="label-extra" name="header" />
                </div>
                <input
                    id={this.uniqueId} v-modelNumber="model"
                    placeholder={this.placeholder}
                    type="number"
                />
        
                {this.help && <div class="help">{this.help}</div>}
            </div>
        )
    },
})

