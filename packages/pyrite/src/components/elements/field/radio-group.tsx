import {defineComponent} from '@/lib/component'
import Field from './field'
import {h} from 'vue'
import {$t} from '@/lib/main'

export const FieldRadioGroup = defineComponent({
    emits: ['update:modelValue'],
    extends: Field,
    methods: {
        updateModel: function(event) {
            this.$emit('update:modelValue', event.target.value)
        },
    },
    props: {
        modelValue: {
            required: true,
            type: String,
        },
        options: {
            required: true,
            type: Array,
        },
    },
    render() {
        return (
            <div class="c-field-radio-group field">
                <div class="checkbox-row">
                    {this.label && <div class="field-label">{this.label}</div>}
                </div>
                <div class="options">
                    {this.options((option) => {
                        return (
                            <div key={option[0]} class="option">
                                <input
                                    id={option[0]}
                                    checked={option[0] === this.modelValue}
                                    name="same"
                                    type="radio"
                                    value={option[0]}
                                    onInput={(e) => this.updateModel(e)}
                                />
                                <label for={option[0]}>{$t(option[1])}</label>
                            </div>
                        )
                    })}

                </div>
                {this.help && !this.invalidFieldValue && <div class="help">{this.help}</div>}
            </div>
        )
    },
})
