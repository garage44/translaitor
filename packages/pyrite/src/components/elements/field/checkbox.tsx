import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import Field from './field'
import {h} from 'vue'

export const FieldCheckbox = defineComponent({
    emits: ['update:modelValue'],
    extends: Field,
    methods: {
        updateModel: function(event) {
            this.$emit('update:modelValue', event.target.checked ? this.toggle[1] : this.toggle[0])
        },
    },
    props: {
        modelValue: {
            required: true,
            type: [Boolean, String],
        },
        toggle: {
            default: () => [false, true],
            required: false,
            type: Array,
        },
    },
    render() {
        return (
            <div class="c-field-checkbox field">
                <div class="checkbox-row">
                    {this.label && <div class="field-label">
                        { this.label }
                    </div>}
                    <label class={classes('switch', {[this.elementclass]: true})} for={this.name}>
                        <input
                            id={this.name}
                            checked={this.modelValue === this.toggle[0] ? false : true}
                            class={this.elementclass}
                            disabled={this.disabled}
                            name={this.name}
                            type="checkbox"
                            onChange={this.updateModel(this.$event)}
                        />
                        <span class="slider" />
                    </label>
                </div>
                <div v-if="help && !invalidFieldValue" class="help">
                    { this.help }
                </div>
            </div>
        )
    },
})
