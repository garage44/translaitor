import {defineComponent} from '@/lib/component'
import Field from './field'
import {h} from 'vue'

export const FieldTextarea = defineComponent({
    emits: ['focus', 'update:modelValue'],
    extends: Field,
    methods: {
        handleKeydown(e) {
            e.stopPropagation()
        },
    },
    async mounted() {
        if (this.autofocus) this.$refs.field.focus()
    },
    props: {
        modelValue: {
            default: () => '',
            type: String,
            required: true,
        },
        placeholder: {
            default: () => '',
            type: String,
            required: false,
        },
    },
    render() {
        return (
            <div class="c-field-textarea field">
                {this.label && <label class="field-label" for={this.name}>{this.label}</label>}
                <textarea
                    placeholder={this.placeholder}
                    onInput={(e) => this.updateModel(e)}
                    onKeydownStop={this.handleKeydown}
                />
                {this.help && <div class="help">{this.help}</div>}
            </div>
        )
    },
})
