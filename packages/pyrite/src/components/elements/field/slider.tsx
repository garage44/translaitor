import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const FieldSlider = defineComponent({
    computed: {
        marginTop() {
            const thumbY = this.track.height - (this.track.height) * (this.modelValue.value / 100)  - this.thumb.height
            if (thumbY > this.track.height) {
                return this.track.height - this.thumb.height
            } else if (thumbY - this.thumb.height < 0) {
                return 0
            } else {
                return thumbY
            }
        },
        positionToValue() {
            return 100 - Math.trunc((this.thumb.y / this.track.height) * 100)
        },
    },
    data() {
        return {
            down: false,
            drag: {
                y: null,
            },
            thumb: {
                height: null,
                width: null,
                y: null,
            },
            track: {
                height: null,
                y: null,
            },
            value: null,
        }
    },
    emits: ['update:modelValue'],
    methods: {
        onClick(doubleClick) {
            // Locked feature is disabled:
            if (this.modelValue.locked === null) return

            if (doubleClick) {
                // Simulate double-click in order to toggle locking a channel.
                if (!this.timeoutId) {
                    this.timeoutId = setTimeout(() => { this.timeoutId = null }, 500)
                } else {
                    clearTimeout(this.timeoutId)
                    this.timeoutId = null
                    this.$emit('update:modelValue', {
                        locked: !this.modelValue.locked,
                        value:  this.modelValue.value,
                    })
                }
            } else {
                this.$emit('update:modelValue', {
                    locked: !this.modelValue.locked,
                    value:  this.modelValue.value,
                })
            }
        },
        setPosition() {
            if (
                this.drag.y >= this.track.y + this.thumb.height &&
                this.drag.y <= (this.track.y  + this.track.height - this.thumb.height)
            ) {
                this.thumb.y = this.drag.y - this.track.y
            } else if (this.drag.y < this.track.y + this.thumb.height) {
                this.thumb.y = 0
            } else if (this.drag.y > this.track.y + this.thumb.height) {
                this.thumb.y = this.track.height
            }
        },
    },
    mounted() {
        this.track.height = this.$refs.track.offsetHeight
        this.track.y = this.$refs.track.getBoundingClientRect().top + document.documentElement.scrollTop

        this.thumb.height =  this.$refs.thumb.offsetHeight
        this.thumb.width = this.$refs.thumb.offsetWidth

        this.$refs.track.addEventListener("mousedown", (e) => {
            document.body.style.cursor = 'ns-resize'
            this.drag.y = e.pageY
            this.down = true
            this.setPosition()
            this.$emit('update:modelValue', {
                locked: this.modelValue.locked,
                value: this.positionToValue,
            })

        })

        this.mouseMove = (e) => {
            if (!this.down) return
            this.track.y = this.$refs.track.getBoundingClientRect().top + document.documentElement.scrollTop
            this.drag.y = e.pageY
            this.setPosition()
            this.$emit('update:modelValue', {
                locked: this.modelValue.locked,
                value: this.positionToValue,
            })
        }
        this.mouseUp = () => {
            document.body.style.cursor = 'default'
            this.down = false
        }

        document.addEventListener('mousemove', this.mouseMove)
        document.addEventListener('mouseup', this.mouseUp)
    },
    unmounted() {
        document.removeEventListener('mousemove', this.mouseMove)
        document.removeEventListener('mouseup', this.mouseUp)
    },
    props: {
        modelValue: {
            required: true,
            type: Object,
        },
    },
    render() {
        return (
            <div class={classes('c-field-slider', {active: this.down})}>
                {this.modelValue.locked && <Icon
                    class="icon icon-xs locked"
                    name="Lock"
                    onClick={this.onClick(false)}
                />}

                <div ref="track" class="track" onClick={this.onClick(true)}>
                    <div ref="thumb" class="thumb" style={{marginTop: `${this.marginTop}px`}} />
                </div>
            </div>
        )
    },
})
