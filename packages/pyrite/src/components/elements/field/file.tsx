import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import Field from './field'
import {Icon} from '@/components/elements'
import {h} from 'vue'

export const FieldFile = defineComponent({
    data() {
        return {
            files: this.modelValue,
            visible: false,
        }
    },
    emits: ['file', 'update:modelValue'],
    extends: Field,
    methods: {
        previewFiles(event) {
            const files = event.target.files
            for (let i = 0; i < files.length; i++) {
                this.$emit('file', files[i])
                this.files.push(files[i].name)
                this.$emit('update:modelValue', [...this.files])
            }
        },
        resetInput() {
            // Used to undo the action that is triggered by
            // adding one or more files.
            if (this.modelValue.length) {
                this.$refs.input.value = ''
                this.$emit('update:modelValue', [])
                this.$emit('file', null)
            }
        },
    },
    props: {
        accept: {
            default: () => '',
            required: true,
            type: String,
        },
        disabled: {
            default: () => false,
            required: false,
            type: Boolean,
        },
        icon: {
            default: () => 'Play',
            required: false,
            type: String,
        },
        modelValue: {
            required: true,
            type:Object,
        },
        tooltip: {
            default: () => '',
            required: false,
            type: String,
        },
    },
    render() {
        return (
            <label class={classes('c-field-file', {disabled: this.disabled})}>
                <Icon v-tip={{content: this.tooltip}} class="icon icon-d" name={this.icon} />
                <input
                    ref="input"
                    accept={this.accept}
                    disabled={this.disabled}
                    name="fileupload"
                    type="file"
                    onChange={this.previewFiles}
                    onClick={this.resetInput}
                />
                {this.label && <div class="field-label" for="fileupload">{this.label}</div>}
            </label>
        )
    },
})
