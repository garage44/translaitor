import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import Field from './field'
import {Icon} from '@/components/elements'
import {nextTick} from 'vue'
import {h} from 'vue'

export const FieldText = defineComponent({
    computed: {
        className() {
            const classes = {}
            if (this.validation) {
                classes.validation = true
                if (this.validation.$invalid && this.validation.$dirty) {
                    classes.invalid = true
                }
            }
            return classes
        },
    },
    data() {
        return {
            visible: false,
        }
    },
    emits: ['focus', 'update:modelValue'],
    extends: Field,
    async mounted() {
        if (this.autofocus) this.$refs.field.focus()
    },
    props: {
        type: {
            default: 'text',
            type: String,
            validator: function(value) {
                return ['password', 'text'].includes(value)
            },
        },
        validation: {
            default: () => null,
            required: false,
            type: Object,
        },
    },
    render() {
        return (
            <div class={classes('c-field-text field', {[this.className]: true})}>
                {this.label && <label class="field-label" for={this.name}>{this.label}</label>}
                <div class="input-container">
                    <input
                        ref="field"
                        autocomplete={this.autocomplete}
                        autofocus={this.autofocus}
                        class={this.elementclass}
                        disabled={this.disabled}
                        placeholder={this.placeholder}
                        readonly={this.readonly}
                        value={this.modelValue}
                        type={(this.type === 'password' && this.visible) ? 'text' : this.type}
                        onClickStop={this.$emit('focus', event)}
                        onInput={(e) => {
                            this.$emit('update:modelValue', `${e.target.value}`)
                        }}
                    />

                    {this.type === 'password' && <div
                        class={classes('eye', {visible: this.visible})}
                        onClick={() => this.visible = !this.visible}
                    >
                        <Icon class={classes('icon icon-s', {active: this.visible})} name="Eye" />
                    </div>}        
                </div>
                {!!this.validation && <div class="validation-message">
                    {this.validation.$invalid && this.validation.$dirty && this.validation.$silentErrors.map((error) => {
                        return (<div class="error">{error.$message }</div>)
                    })}
                </div>}
                {!!this.help && <div class="help">{this.help}</div>}
            </div>
        )
    },
    watch: {
        async 'autofocus'(value) {
            if (value) {
                await nextTick()
                this.$refs.field.focus()
            }
        },
    },
})
