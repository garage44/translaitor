import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const Progress = defineComponent({
    props: {
        boundaries: {
            type: Array,
            default() {return []},
        },
        percentage: {
            type: Number,
            default() {return 0},
        },
    },
    render() {
        return (
            <div class="c-progress">
                <div class="bar" style={`width:${this.percentage}%`} />
                <div v-if="boundaries.length" class="boundaries">
                    <div class="left">
                        {this.boundaries[0]}
                    </div>
                    <div class="right">
                        {this.boundaries[1]}
                    </div>
                </div>
            </div>
        )
    },
})

