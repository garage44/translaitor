import {classes} from '@/lib/utils'
import {defineComponent} from '@/lib/component'
import {h} from 'vue'

export const ButtonGroup = defineComponent({
    props: {
        active: {
            type: Boolean,
            required: true,
        },
    },
    render() {
        return (
            <div class={classes('c-button-group', {active: this.active})}>
                <slot />
            </div>
        )
    },
})
