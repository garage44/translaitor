import * as modelChat from '@/models/chat'
import * as modelGroup from '@/models/group'
import * as modelMedia from '@/models/media'
import * as modelSFU from '@/models/sfu/sfu'
import * as modelUser from '@/models/user'

import animate from '@/lib/animate'
import Api from '@/lib/api'
import App from '@/components/elements/app'
import {createApp, h} from 'vue'
import {createI18n} from 'vue-i18n'
import env from '@/lib/env.js'
import EventEmitter from 'eventemitter3'
import Logger from '@/lib/logger.js'
import Notifier from '@/lib/notifier.js'
import Store from '@/lib/store.js'
import VueTippy from 'vue-tippy'

class Pyrite extends EventEmitter {
    animate = animate

    api = new Api()

    logger = new Logger(this)

    $m = {
        chat: modelChat,
        group: modelGroup,
        media: modelMedia,
        sfu: modelSFU,
        user: modelUser,
    }

    store = new Store()

    constructor() {
        super()

        this.logger.setLevel('debug')
        this.logger.debug('loading store')
        // Usage on the import level (e.g. define computed properties)
        // still requires separate model imports.
        this.$s = this.store.load()
        env(this.$s.env)

        this.notifier = Notifier(this)
        this.setI18n()
    }

    async adminContext() {
        const context = await this.api.get('/api/context')
        Object.assign(this.$s.admin, context)
    }

    async init(router) {
        await this.adminContext()

        this.router = router(this)
        this.router.beforeResolve((to, from, next) => {
            // All admin routes are authenticated. Redirect to the admin
            // login if the authentication flag is unset.
            if ((to.name && to.name !== 'admin-login' && to.name.startsWith('admin')) && !this.$s.admin.authenticated) {

                next({name: 'admin-login'})
                return
            }

            if (!this.$s.group.connected) {
                // Navigating groups will change the internally used groupId;
                // but only when not connected to a group already.
                if (to.name === 'conference-groups-disconnected') {
                    this.$s.group.name = to.params.groupId
                }
            } else if (to.name === 'admin-group') {
                this.$s.admin.group._name = to.params.groupId
            }
            next()
        })
        this.vm = createApp(App)
        this.vm.use(VueTippy, {
            component: 'tip',
            defaultProps: {
                allowHTML: true,
                appendTo: () => {
                    return document.querySelector('.app')
                },
                delay: [750, 100],
            },
            directive: 'tip',
            interactive: true,
        }),

        Object.assign(this.vm.config.globalProperties, {
            $m: this.$m,
            $s: this.$s,
            app: this,
        })

        this.vm.directive('click-outside', {
            beforeMount(el, binding) {
                el.clickOutsideEvent = function(event) {
                    if ((el !== event.target) && !el.contains(event.target)) {
                        binding.value(event, el)
                    }
                }
                document.body.addEventListener('click', el.clickOutsideEvent)
            },
            unmounted(el) {
                document.body.removeEventListener('click', el.clickOutsideEvent)
            },
        })

        this.vm.use(this.router).use(this.i18n)
        this.vm.mount('#app')
    }

    async setI18n() {
        // Language preference is not set yet; use the browser default if
        // supported; english as final fallback otherwise.
        let currentLanguage = 'en'
        if (this.$s.language.id) {
            currentLanguage = this.$s.language.id
        } else {
            const supportedLanguages = ['de', 'en', 'nl', 'fr']
            const browserLanguage = navigator.language.split('-')[0]
            if (supportedLanguages.includes(browserLanguage)) {
                currentLanguage = browserLanguage
            } else {
                currentLanguage = 'en'
            }
        }

        this.i18n = createI18n({
            formatFallbackMessages: true,
            locale: this.$s.language.id,
            messages: {},
            silentFallbackWarn: true,
            silentTranslationWarn: true,
        })

        this.$t = this.i18n.global.t
        this.$tc = this.i18n.global.tc
        this.setLanguage(currentLanguage)
    }

    async setLanguage(language) {
        const i18nTags = await this.api.get(`/api/i18n/${language}`)
        this.i18n.global.setLocaleMessage(language, i18nTags)
        app.i18n.global.locale = language
    }
}

export const app = new Pyrite()
export const $s = app.$s
export const $t = app.$t
export const $tc = app.$tc
