import {app} from '@/lib/main.js'

export default function() {
    return new class Notify {

        messages = {
            clearchat: {level: 'warning'},
            error: {level: 'error'},
            kicked: {level: 'warning'},
            lock: {level: 'warning'},
            mute: {level: 'warning'},
            notification: {level: 'info'},
            present: {level: 'info'},
            raisehand: {level: 'info'},
            record: {level: 'warning'},
            unlock: {level: 'warning'},
            unrecord: {level: 'warning'},
        }

        /**
         * A shared entrypoint for messages that have a
         * source and a target message. Use `context.dir`
         * to assign the message direction.
         * @param {*} messageId
         * @param {*} context
         * @param {*} personal
         * @param {*} channels
         */
        message(messageId, context = {dir: 'target', icon: 'Info'}, personal = null, {chat = false, notification = true} = {}) {
            if (notification) {
                if (!context.dir) context.dir = 'target'

                this.notify({
                    icon: context.icon,
                    level: this.messages[messageId] ? this.messages[messageId].level : 'info',
                    message: app.$t(`message.${messageId}.${context.dir}`, context),
                    personal,
                })
            }

            if (chat) {
                let chatMessage =app.$t(`message.${messageId}.chat`)
                if (personal) chatMessage = `${chatMessage} (${personal.message})`
                app.$m.sfu.connection.chat('me', '', chatMessage)
            }
        }

        notify(notification, timeout = 5000) {
            if (!this.notificationId) {
                this.notificationId = 1
                notification.id = this.notificationId
            }
            app.$s.notifications.push(notification)
            if (timeout) {
                this.setTimeout(notification, timeout)
            }

            this.notificationId += 1
            return app.$s.notifications.find((i) => i.id === notification.id)
        }

        onUserMessage({kind, message, privileged, source}) {
            if (!(kind in this.messages)) return
            // All notifications are triggered from privileged actions at the moment.
            if (!privileged) return

            if (kind === 'error') {
                // Ignore this message; this happens when a stream can
                // no longer be played due to revoked present permission.
                // This is dealt with at another place.
                if (message === 'permission denied') return
                this.notify({level: 'error', message})
            } else if (kind === 'info') {
                this.notify({level: 'info', message})
            }

            if (kind === 'kicked') {
                // No personal message if its the default message...
                if (message !== 'you have been kicked out') {
                    this.notify({
                        level: this.messages[kind] ? this.messages[kind].level : 'info',
                        message: app.$t(`message.${kind}.target`, {group: app.$s.group.name, message, source}),
                        personal: {message, op: source},
                    })
                }
            } else {
                this.notify({
                    level: this.messages[kind] ? this.messages[kind].level : 'info',
                    message: app.$t(`message.${kind}.target`, {group: app.$s.group.name, message, source}),
                    personal: message ? {message, source} : null,
                })
            }

        }

        setTimeout(notification, timeout = 5000) {
            setTimeout(() => {
                app.$s.notifications.splice(app.$s.notifications.findIndex(i => i.id === notification.id), 1)
            }, timeout)
        }
    }
}
