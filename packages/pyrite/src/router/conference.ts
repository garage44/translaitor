import App from '@/components/conference/app'

import {app} from '@/lib/main.js'
import {Group} from '@/components/conference/group/group'
import {Login} from '@/components/conference/login/login'
import Settings from '@/components/conference/settings/settings'
import {Splash} from '@/components/elements'

export default [{
    children: [
        {
            component: Splash,
            name: 'conference-splash',
            path: '/',
        },
        {
            // Settings accessible when not in a group yet
            component: Settings,
            name: 'conference-settings',
            path: '/settings/:tabId',
        },
        {
            name: 'conference-main',
            path: '/',
            redirect: () => {
                // Don't allow to route to the splash-screen while
                // being connected to a group.
                if (app.$s.group.connected) {
                    return {name: 'conference-groups', params: {groupId: app.$s.group.name}}
                } else {
                    return {name: 'conference-splash'}
                }
            },
        },
        {
            name: 'conference-groups',
            path: '/groups/:groupId',
            redirect: () => {
                if (app.$s.group.connected) return {name: 'conference-groups-connected'}
                return {name: 'conference-groups-disconnected'}
            },
        },
        {
            children: [
                {
                    // Settings accessible when not in a group yet
                    beforeEnter: (to, from, next) => {
                        if (!app.$s.group.connected) {
                            next({name: 'conference-settings', params: {tabId: 'misc'}})
                        } else {
                            next()
                        }
                    },
                    component: Settings,
                    name: 'conference-group-settings',
                    path: 'settings/:tabId',
                },
            ],
            component: Group,
            name: 'conference-groups-connected',
            path: '/groups/:groupId',
        },
        {
            component: Login,
            name: 'conference-groups-disconnected',
            path: '/groups/:groupId/login',
        },
    ],
    component: App,
    name: 'conference',
    path: '/',
}]
