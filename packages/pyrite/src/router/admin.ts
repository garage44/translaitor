import App from '@/components/admin/app'
import {app} from '@/lib/main.js'

import Groups from '@/components/admin/groups'
import GroupSettings from '@/components/admin/group/settings/settings'
import {Login} from '@/components/admin/login'
import Users from '@/components/admin/users/users'
import UserSettings from '@/components/admin/users/settings/settings'

export default [{
    beforeEnter: () => {
        app.$s.admin.group = null
    },
    children: [
        {
            component: Login,
            name: 'admin-login',
            path: '/admin/login',
        },
        {
            children: [
                {
                    component: GroupSettings,
                    name: 'admin-groups-settings',
                    path: '/admin/groups/settings/:groupId?/:tabId?',
                    props: true,
                },
            ],
            component: Groups,
            name: 'admin-groups',
            path: '/admin/groups',
            props: true,
        },
        {
            children: [
                {
                    component: UserSettings,
                    name: 'admin-users-settings',
                    path: '/admin/users/settings/:userId?',
                    props: true,
                },
            ],
            component: Users,
            name: 'admin-users',
            path: '/admin/users',
            props: true,
        },
    ],
    component: App,
    name: 'admin',
    path: '/admin',
}]
