import './lib/helpers'
import {app} from './lib/main'
import {h} from 'vue'
import router from './router/router'

globalThis.h = h

app.init(router)

app.setLanguage('en')
globalThis.app = app

for (const model of Object.values(app.$m)) {
    if (model._events) model._events()
}
