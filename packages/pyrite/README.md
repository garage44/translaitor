![Pyrite build passing](https://github.com/garage44/pyrite/actions/workflows/test.yml/badge.svg)
<a href="https://hosted.weblate.org/engage/pyrite/">
<img src="https://hosted.weblate.org/widgets/pyrite/-/svg-badge.svg" alt="Translation status" />
</a>
<br /><br />
<img height="100" src="./ui/public/logo-text.svg">
<br />

[Galène](https://galene.org/) is a videoconference server (an “SFU”) that is easy
to deploy and that requires moderate server resources. [Pyrite](https://pyrite.video)
is an alternative web client and management interface for Galène, based on the
[Vue](https://v3.vuejs.org/) framework. Read the [docs](./docs/index.md)
for more background info, such as [features](./docs/features.md).

# Getting Started

```bash
git clone https://github.com/garage44/pyrite
cd pyrite/docker
id # Find out your host user/group id; used to keep volume permissions sane
PYRITE_UID=1000 PYRITE_GID=1000 docker-compose up
```

* Open a browser: <http://localhost:3030>
* Click on the logo to switch to admin modus
* Login with the pyrite admin credentials listed in the initial logs
* Setup a group and add some users to it

:tada: That's all to it! Switch back to conferencing mode to login to a group.
Checkout the [docs](./docs/index.md) for more information about setting up a
networked Pyrite environment, so people from other computers can join your
conference.

## Development

This is a shortcut on how to setup Pyrite, in case you're interested in
its development stack:

```bash
git clone https://github.com/jech/galene
cd galene
git checkout galene-0.5.1
CGO_ENABLED=0 go build -ldflags='-s -w'
mkdir -p {data,groups,recordings}
./galene --insecure
```

```bash
# Using the published version:
npx @garage44/pyrite:latest
# Or manually run the dev-stack:
git clone https://github.com/garage44/pyrite
cd pyrite
npm install
npm run dev  # vitejs dev-service: http://localhost:3000
nodemon backend/app.js  # Express backend: http://localhost:3030
```

## Deployment

```
After=network.target

[Service]
Type=simple
User=pyrite
Group=pyrite
ExecStart=npx @garage44/pyrite@latest
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target

```

<a href="https://hosted.weblate.org/engage/pyrite/">
    <img src="https://hosted.weblate.org/widgets/pyrite/-/open-graph.png" width="200" alt="Translation status" />
</a>
