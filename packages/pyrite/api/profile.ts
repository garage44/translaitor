import {authContext, noAuthContext, noPermissionContext} from '../backend/lib/profile.js'
import {loadUser, loadUsers} from '../backend/lib/user.js'

export default function(app) {

    app.get('/api/context', async function(req, res) {
        const session=req.session
        let context

        // Check if security is disabled via environment variable
        if (process.env.PYRITE_NO_SECURITY) {
            app.logger.warn('session security is disabled (PYRITE_NO_SECURITY)')
            context = await authContext()
            return res.end(JSON.stringify(context))
        }

        // Check if the session has a user ID
        if (session.userid) {
            const user = await loadUser(session.userid)
            // If user is not found or not an admin, return noAuthContext
            if (!user || !user.admin) {
                context = noAuthContext()
            } else {
                // If user is an admin, return authContext
                context = await authContext()
            }
        } else {
            // If no user ID in session, return noAuthContext
            context = noAuthContext()
        }
        res.end(JSON.stringify(context))
    })

    app.post('/api/login', async function(req, res) {
        const users = await loadUsers()
        const username = req.body.username
        const user = users.find((i) => i.name === username)
        let context

        if (!user) {
            context = noAuthContext()
            res.end(JSON.stringify(context))
        } else {
            const password = req.body.password

            if (password === user.password) {
                if (user.admin) {
                    const session = req.session
                    session.userid = user.id
                    context = await authContext()
                } else {
                    context = noPermissionContext()
                }
            } else {
                context = noAuthContext()
            }
        }
        res.end(JSON.stringify(context))
    })

    app.get('/api/logout',async(req, res) => {
        console.log('LOGOUT')
        req.session.destroy()
        const context = noAuthContext()
        res.end(JSON.stringify(context))
    })
}
