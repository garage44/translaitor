import fs from 'fs-extra'
import path from 'path'

const acceptedLanguages = [
    'de',
    'en',
    'nl',
    'fr',
]

export default async function(app) {

    app.get('/api/i18n/:language', async function(req, res) {
        const language = req.params.language
        if (acceptedLanguages.includes(language)) {
            const i18nTags = await fs.promises.readFile(path.join(app.settings.dir.admin, 'i18n', `${language}.json`), 'utf8')
            res.end(i18nTags)
        } else {
            res.status(404).send({error: 'invalid language'})
        }
    })
}
